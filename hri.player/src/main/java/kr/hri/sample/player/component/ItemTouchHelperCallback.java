package kr.hri.sample.player.component;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import kr.imgtech.lib.zoneplayer.itemtouchhelper.ItemTouchHelperExtension;

/**
 * ItemTouchHelper 콜백 클래스
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class ItemTouchHelperCallback extends ItemTouchHelperExtension.Callback {

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        return makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.START);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

        CourseAdapter adapter = (CourseAdapter) recyclerView.getAdapter();
        adapter.move(viewHolder.getLayoutPosition(), target.getLayoutPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

    }

    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        //super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

        if (viewHolder != null) {

            if (dY != 0 && dX == 0)  {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            if (viewHolder instanceof CourseAdapter.ItemBaseViewHolder) {
                ((CourseAdapter.ItemBaseViewHolder) viewHolder).mLayoutCourse.setTranslationX(dX);
            } else if (viewHolder instanceof LectureAdapter.ItemBaseViewHolder) {
                ((LectureAdapter.ItemBaseViewHolder) viewHolder).mLayoutLecture.setTranslationX(dX);
            }
        }

    }
}
