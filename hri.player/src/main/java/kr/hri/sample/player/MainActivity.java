package kr.hri.sample.player;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.andlib.helpers.Logger;

import java.net.URISyntaxException;
import java.util.ArrayList;

import kr.hri.sample.player.widget.Util;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.Lib;
import kr.imgtech.lib.zoneplayer.util.StringUtil;

public class MainActivity extends AppCompatActivity implements BottomNavigationBar.OnTabSelectedListener, IntentDataDefine {

    public final static int REQUEST_CODE = 1004;

    // test user-id
    // todo 테스트 다운로드 데이터와 동일하게 guest 로 설정. 다운로드 데이터가 변경되면 같이 변경 요망
    public final static String GUEST = "guest";
    public final static String HRI_ID = "62";

    public final static String KEY_INTENT = "intent";
    public final static String KEY_REQUEST_URL = "request-url";
    public final static String KEY_DELETE_URL = "delete-url";

    public final static int ID_TEST = 1;
    public final static int ID_BOX_COURSE = 11;
    public final static int ID_BOX_LECTURE = 12;
    public final static int ID_DOWNLOAD = 2;
    public final static int ID_SETTINGS = 3;

    private BottomNavigationBar mBottomNavigationBar;
    private IBackPressedListener mBackPressedListener;
    private FloatingActionButton mFAB;

    private Handler mHandler = new Handler();

    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {

        if (requestCode == REQUEST_CODE) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    Lib.toaster(MainActivity.this, "Overlay Permission Granted");
                } else {
                    Lib.toaster(MainActivity.this, "Overlay Permission Denied");
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConfigurationManager.setAllowCapture(getApplicationContext(), true);

        PermissionListener permissionListener = new PermissionListener() {

            @Override
            public void onPermissionGranted() {
                //Lib.toaster(MainActivity.this, "Permission Granted");
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Lib.toaster(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString());
            }
        };

        startOverlayWindowService(getApplicationContext());

        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service" +
                        "\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();

        mBottomNavigationBar = findViewById(R.id.navigation);

        mBottomNavigationBar.setTabSelectedListener(this);

        mBottomNavigationBar
                .setMode(BottomNavigationBar.MODE_SHIFTING);
        mBottomNavigationBar
                .setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_RIPPLE);
        mBottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.ic_play_circle_outline, R.string.title_box).setActiveColorResource(R.color.colorAccent))
                .addItem(new BottomNavigationItem(R.drawable.ic_inbox, R.string.title_box).setActiveColorResource(R.color.cyan_500))
                .addItem(new BottomNavigationItem(R.drawable.ic_move_to_inbox, R.string.title_download).setActiveColorResource(R.color.indigo_500))
                .addItem(new BottomNavigationItem(R.drawable.ic_settings, R.string.title_settings).setActiveColorResource(R.color.grey_600))
                .setFirstSelectedPosition(0)
                .initialise();

        mFAB = findViewById(R.id.fab_download);
        //mBottomNavigationBar.setFab(mFAB);

        Intent intent = getIntent();
        if ((intent != null) && (intent.getData() != null)) {

            if (StringUtil.equals(intent.getData().getHost(), getString(R.string.host_download_working))) {

                String downloadReqURL = DownloadRequestActivity.getDownloadReq(getApplicationContext());

                if (StringUtil.isNotBlank(downloadReqURL)) {

                    Intent downloadIntent = null;
                    try {
                        downloadIntent = Intent.parseUri(downloadReqURL, Intent.URI_INTENT_SCHEME);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(KEY_INTENT, downloadIntent);

                    DownloadRequestActivity.setDownloadReq(getApplicationContext(), "");

                    switchBottomNavigationBar(ID_DOWNLOAD);
                    switchFragment(ID_DOWNLOAD, bundle);
                } else {

                    switchBottomNavigationBar(ID_DOWNLOAD);
                    switchFragment(ID_DOWNLOAD, null);
                }
            }
        } else {

            switchFragment(ID_TEST, null);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if ((intent != null) && (intent.getData() != null)) {

            if (StringUtil.equals(intent.getData().getHost(), getString(R.string.host_download_working))) {

                // 다운로드 요청 수신 Activity 로부터 다운로드 요청 URL 반환
                String downloadReqURL = DownloadRequestActivity.getDownloadReq(getApplicationContext());

                // 다운로드 요청 URL 이 유효하면
                if (StringUtil.isNotBlank(downloadReqURL)) {

                    // 다운로드 요청 URL 을 parseUri
                    Intent downloadIntent = null;
                    try {
                        downloadIntent = Intent.parseUri(downloadReqURL, Intent.URI_INTENT_SCHEME);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    // Intent 를 Bundle 에 설정
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(KEY_INTENT, downloadIntent);

                    // 다운로드 요청 수신 Activity 에서 요청 데이터 초기화
                    DownloadRequestActivity.setDownloadReq(getApplicationContext(), "");

                    // UI 설정 및 기타 처리와 함께 Fragment (DownloadManager4) 에 전달하고 FragmentTransaction 실행
                    switchBottomNavigationBar(ID_DOWNLOAD);
                    switchFragment(ID_DOWNLOAD, bundle);
                } else {

                    switchBottomNavigationBar(ID_DOWNLOAD);
                    switchFragment(ID_DOWNLOAD, null);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (mBackPressedListener != null) {
            mBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTabSelected(int position) {

        switch (position) {

            case 0:
                switchFragment(ID_TEST, null);
                break;
            case 1:
                switchFragment(ID_BOX_COURSE, null);
                break;
            case 2:
                switchFragment(ID_DOWNLOAD, null);
                break;
            case 3:
                switchFragment(ID_SETTINGS, null);
                break;
        }
    }

    @Override
    public void onTabUnselected(int position) {

    }

    @Override
    public void onTabReselected(int position) {

    }

    /**
     * Application 종료 일괄 처리
     */
    public void finishApplicationProcess() {

        if (Util.isSnack()) {

            Util.dismissSnack();
        } else {

            Util.snack(mBottomNavigationBar, R.string.toast_finish_confirm, 4000,
                    R.string.close, new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            finish();
                        }
                    });
        }

    }

    /**
     * BottomNavigationBar 반환
     * @return  BottomNavigationBar
     */
    public BottomNavigationBar getBottomNavigationBar() {

        return mBottomNavigationBar;
    }

    /**
     * FloatingActionButton 반환
     * @return  BottomNavigationBar
     */
    public FloatingActionButton getFloatingActionButton() {

        return mFAB;
    }

    public void switchBottomNavigationBar(int id) {

        switch (id) {

            case ID_TEST:
                mBottomNavigationBar.selectTab(0);
                break;

            case ID_BOX_COURSE:
            case ID_BOX_LECTURE:
                mBottomNavigationBar.selectTab(1);
                break;

            case ID_DOWNLOAD:
                mBottomNavigationBar.selectTab(2);
                break;

            case ID_SETTINGS:
                mBottomNavigationBar.selectTab(3);
                break;
        }
    }

    /**
     * Fragment 변경 Replace
     * @param id		변경할 화면 ID
     * @param bundle	Fragment 에 전달할 Intent Bundle
     */
    public void switchFragmentDelay(final int id, final Bundle bundle) {

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {

                switchFragment(id, bundle);
            }

        }, 200);
    }

    /**
     * Fragment 변경 Replace
     * @param id		변경할 화면 ID
     * @param bundle	Fragment 에 전달할 Intent Bundle
     */
    @SuppressLint("RestrictedApi")
    public void switchFragment(int id, Bundle bundle) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ftx = fragmentManager.beginTransaction();

        Fragment fragment;
        if (mBackPressedListener != null) {
            mBackPressedListener = null;
        }

        switch (id) {

            case ID_TEST:

                fragment = MainFragment.getInstance();
                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;

                mFAB.setVisibility(View.GONE);
                break;

            case ID_BOX_COURSE:

                Bundle courseBundle = new Bundle();
                courseBundle.putParcelable(MainActivity.KEY_INTENT, makeCourseIntent(HRI_ID, GUEST));

                fragment = CourseFragment.getInstance(courseBundle);
                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;

                mFAB.setVisibility(View.GONE);
                break;

            case ID_BOX_LECTURE:

                fragment = LectureFragment.getInstance(bundle);
                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;

                mFAB.setVisibility(View.GONE);
                break;

            case ID_DOWNLOAD:

                fragment = DownloadManager6.getInstance(bundle);
                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;

                mFAB.setVisibility(View.GONE);
                break;

            case ID_SETTINGS:

                fragment = SettingsFragment.getInstance();
                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;

                mFAB.setVisibility(View.GONE);
                break;
        }

        ftx.commitAllowingStateLoss();
    }

    /**
     * Course Group Fragment Intent 생성
     * @param siteID	Site ID
     * @param userID    User ID
     * @return	Course Group Fragment Intent
     */
    private Intent makeCourseIntent(String siteID, String userID) {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority("course-list")
                .appendQueryParameter(SITE_ID, siteID)
                .appendQueryParameter(USER_ID, userID)
                ;

        Logger.d(uriBuilder.toString());

        Intent intent = null;

        // Intent 설정
        try {
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getPackageName());
            intent.setPackage(getPackageName());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return intent;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void startOverlayWindowService(Context context) {

        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) && (! Settings.canDrawOverlays(context))) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
        }
    }
}
