package kr.hri.sample.player.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import kr.hri.sample.player.R;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.Extension;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.ItemTouchHelperExtension;
import kr.imgtech.lib.zoneplayer.util.Lib;

/**
 * 강의 어댑터
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class LectureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ZoneDownloadData> mLectureList;
    private ItemTouchHelperExtension mItemTouchHelperExtension;
    private LectureAdapterListener mListener;

    /**
     * 생성자
     * @param context   Context
     */
    public LectureAdapter(Context context) {

        mContext = context;
        mLectureList = new ArrayList<>();

        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.row_lecture_extension, parent, false);

        return new ItemSwipeWithActionWidthViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        ItemBaseViewHolder baseViewHolder = (ItemBaseViewHolder) holder;
        baseViewHolder.bind(mLectureList.get(position));
        baseViewHolder.mLayoutLecture.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //Lib.toaster(mContext, "select: " + holder.getLayoutPosition());

                if (mListener != null) {
                    mListener.onItemSelectedListener(holder.getLayoutPosition());
                }
            }
        });

        baseViewHolder.mLayoutLecture.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    if (mListener != null) {
                        mListener.onDeleteSelectedListener(holder.getLayoutPosition());
                    }

                    return true;
                }
            }
        );

        ItemSwipeWithActionWidthViewHolder viewHolder = (ItemSwipeWithActionWidthViewHolder) holder;
        viewHolder.mActionViewDelete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //Lib.toaster(mContext, "delete: " + holder.getLayoutPosition());

                if (mListener != null) {
                    mListener.onDeleteSelectedListener(holder.getLayoutPosition());
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return mLectureList.size();
    }

    /**
     * 선택 리스너 설정
     * @param listener  선택 리스너
     */
    public void setOnSelectedListener(LectureAdapterListener listener) {

        mListener = listener;
    }

    /**
     * 리스트 설정
     * @param list  리스트
     */
    public void setCourseList(List<ZoneDownloadData> list) {

        mLectureList.clear();
        mLectureList.addAll(list);
    }

    /**
     * 리스트 업데이트
     * @param list  리스트
     */
    public void updateList(List<ZoneDownloadData> list) {

        setCourseList(list);
        notifyDataSetChanged();
    }

    /**
     * 리스트 반환
     * @return  리스트
     */
    public ArrayList<ZoneDownloadData> getCourseList() {

        return (ArrayList<ZoneDownloadData>) mLectureList;
    }

    /**
     * 아이템 반환
     * @param position  반환 position
     * @return  아이템
     */
    public ZoneDownloadData getItem(int position) {

        return mLectureList.get(position);
    }

    /**
     * ItemTouchHelperExtension 설정
     * @param extension ItemTouchHelperExtension
     */
    public void setItemTouchHelperExtension(ItemTouchHelperExtension extension) {

        mItemTouchHelperExtension = extension;
    }

    /**
     * Item 추가
     * @param item	추가할 Item
     */
    public void addItem(ZoneDownloadData item) {

        mLectureList.add(item);
        notifyItemInserted(mLectureList.size() - 1);
    }

    /**
     * Index 설정하고 Item 추가
     * @param position	추가할 Index
     * @param item		추가할 Item
     * @return 추가된 Item Index
     */
    public int addItem(int position, ZoneDownloadData item) {

        if (position > mLectureList.size()) {
            position = mLectureList.size();
        }
        mLectureList.add(position, item);
        notifyItemInserted(position);

        return position;
    }

    /**
     * Item 삭제
     * @param item	삭제할 Item
     */
    public void removeItem(ZoneDownloadData item) {

        int position = mLectureList.indexOf(item);
        mLectureList.remove(position);

        notifyItemRemoved(position);
    }

    /**
     * 아이템 순서 변경
     * @param from  기존 position
     * @param to    신규 position
     */
    public void move(int from, int to) {

        Collections.swap(mLectureList, from, to);
        notifyItemMoved(from, to);
    }

    /**
     * Time 을 날짜 형식 문자로 반환
     * @param date	Time (초 단위)
     * @return 날짜 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDate(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return String.format("%04d.%02d.%02d", year, mon + 1, day);
    }

    /**
     * 최종 Course Row ViewHolder
     */
    class ItemSwipeWithActionWidthViewHolder extends ItemBaseViewHolder implements Extension {

        View mActionViewDelete;

        /**
         * 생성자
         * @param itemView Extension View
         */
        ItemSwipeWithActionWidthViewHolder(View itemView) {

            super(itemView);
            mActionViewDelete = itemView.findViewById(R.id.iv_action_delete);
        }

        @Override
        public float getActionWidth() {
            return mLayoutExtension.getWidth();
        }
    }

    /**
     * 기본 Course Row ViewHolder
     */
    class ItemBaseViewHolder extends RecyclerView.ViewHolder {

        TextView tvLectureSeq;
        TextView tvLectureName;
        TextView tvDurationEndTime;
        ProgressBar pbProgress;
        View layoutLecturePlay;
        TextView tvVideoQuality;

        View mLayoutLecture;
        View mLayoutExtension;

        /**
         * 생성자
         * @param itemView  Course Row View
         */
        ItemBaseViewHolder(View itemView) {

            super(itemView);

            tvLectureSeq = (TextView) itemView.findViewById(R.id.tv_lecture_seq);
            tvLectureName = (TextView) itemView.findViewById(R.id.tv_lecture_name);
            tvDurationEndTime = (TextView) itemView.findViewById(R.id.tv_duration_end_time);
            pbProgress = (ProgressBar) itemView.findViewById(R.id.pb_progress);
            layoutLecturePlay = itemView.findViewById(R.id.layout_lecture_play);
            tvVideoQuality = (TextView) itemView.findViewById(R.id.tv_video_quality);

            mLayoutLecture = itemView.findViewById(R.id.row_lecture);
            mLayoutExtension = itemView.findViewById(R.id.row_action);
        }

        @SuppressLint("SetTextI18n")
        void bind (ZoneDownloadData data) {

            tvLectureSeq.setText(Integer.toString(data.lectureSeq + 1));    // 0부터 시작이므로
            tvLectureName.setText(data.lectureName);

            if (data.isCert > 0) {

                // todo duration time + end time
                long duration;
                try {
                    duration = Long.parseLong(data.durationTime) * 1000L;
                    tvDurationEndTime.setText(Lib.millisToText(duration) + " / " + getFormatDate(data.certEndTime));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    tvDurationEndTime.setText(getFormatDate(data.certEndTime));
                }

            } else {

                // todo duration time + end time
                long duration;
                try {
                    duration = Long.parseLong(data.durationTime) * 1000L;
                    tvDurationEndTime.setText(Lib.millisToText(duration) + " / " + mContext.getString(R.string.no_expired));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    tvDurationEndTime.setText(mContext.getString(R.string.no_expired));
                }
            }

            // todo 진도율 설정
            pbProgress.setProgress(data.progressRate);

            tvVideoQuality.setText(data.videoQualityName);
        }
    }

    /**
     * 어댑터 아이템 선택 리스너
     */
    public interface LectureAdapterListener {

        /**
         * 아이템 선택 반환
         * @param position  선택 아이템 position
         */
        void onItemSelectedListener(int position);

        /**
         * 삭제 선택 반환
         * @param position  삭제 아이템 position
         */
        void onDeleteSelectedListener(int position);
    }
}
