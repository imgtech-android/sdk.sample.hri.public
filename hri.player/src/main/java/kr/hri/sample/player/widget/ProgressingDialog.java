package kr.hri.sample.player.widget;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;

/**
 * ProgressBar DialogFragment
 * @author kimsanghwan
 * @since 2014. 11. 23.
 */
public class ProgressingDialog extends DialogFragment implements BaseInterface {
	
	public final static String DIALOG_TAG = "progressdialog";
	public final static int DIALOG_CHOICE_POSITIVE = 1;			// 확인 등 실행의 선택
	public final static int DIALOG_CHOICE_NEUTRAL = 0;			// 중립 선택
	public final static int DIALOG_CHOICE_NEGATIVE = -1;		// 아니오 등 거부의 선택
	public final static int DIALOG_CANCEL = 2;					// 버튼 선택없이 백버튼으로 취소
	
	private final static String KEY_STATE = "state";			// 다이얼로그 상태키
	
	private int state;									// 다이얼로그 상태
	
	/**
	 * 생성자
	 */
	public ProgressingDialog() {
		super();
	}
	
	/**
	 * 다이얼로그 생성 및 반환
	 * @param state	다이얼로그 상태
	 * @return	ProgressingDialog
	 */
	public static ProgressingDialog getInstance(int state) {

		ProgressingDialog dialog = new ProgressingDialog();
		
		Bundle args = new Bundle();
		args.putInt(KEY_STATE, state);
		
		dialog.setArguments(args);
		
		return dialog;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		ProgressDialog dialog = new ProgressDialog(getActivity());
		
		Bundle arguments = getArguments();
		
		state = arguments.getInt(KEY_STATE);

		switch (state) {

		default:
			dialog.setMessage("잠시만 기다려 주세요.");
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setCancelable(false);
			break;

		}

		return dialog;
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	/**
	 * 다이얼로그 표출시 백버튼 터치 이벤트
	 * 백버튼 터치 시 이벤트 수신
	 */
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		
		switch (state) {
		
		case DIALOG_DATA_INFO_ERROR:
		case DIALOG_MEDIA_ERROR:
		case DIALOG_CONTINUE_YES_NO:
		case DIALOG_NETWORK_WARNING:
		case DIALOG_END_REACHED:
		case DIALOG_DOWNLOAD_COMPLETE:
		case DIALOG_DOWNLOAD_CANCEL:

			break;
		}
	}
	
}
