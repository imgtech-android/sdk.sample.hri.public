package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.andlib.helpers.Logger;

import java.net.URISyntaxException;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;

public class MainFragment extends Fragment implements IntentDataDefine, IBackPressedListener, View.OnClickListener  {

    @SuppressLint("StaticFieldLeak")
    private static MainFragment instance;

    private Button mBtnPlay;
    private Button mBtnDownload;

    private Handler handler = new Handler();    // 토스트 팝업을 위한 핸들러

    /**
     * 생성자
     */
    public MainFragment() {
        super();
    }

    /**
     * MainFragment 반환
     * @return  MainFragment
     */
    public static MainFragment getInstance() {

        if (instance == null) {
            instance = new MainFragment();
        }

        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return initView(inflater);
    }


    @Override
    public void onBackPressed() {

        ((MainActivity)getActivity()).finishApplicationProcess();
    }

    @Override
    public void onClick(View view) {

        if (view == mBtnPlay) {
            testPlayer();
        } else if (view == mBtnDownload) {
            testDownload();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private View initView(LayoutInflater inflater) {

        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.fragment_main, null);

        mBtnPlay = view.findViewById(R.id.btn_play);
        mBtnPlay.setOnClickListener(this);

        mBtnDownload = view.findViewById(R.id.btn_download);
        mBtnDownload.setOnClickListener(this);

        return view;
    }

    /**
     * 테스트 앱 - 스트리밍 실행
     */
    private void testPlayer() {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority(getString(R.string.host_player))
                .appendQueryParameter(SITE_ID, MainActivity.HRI_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "play;guest;이명학.Prestart")
                ;

        Logger.d(uriBuilder.toString());

        // 플레이어 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
            intent.setPackage(getActivity().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * 테스트 앱 - 다운로드 실행
     */
    private void testDownload() {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority(getString(R.string.host_download))
                .appendQueryParameter(SITE_ID, MainActivity.HRI_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "download;guest;안철우 선생님")
                ;

        Logger.d(uriBuilder.toString());

        // 플레이어(다운로드) 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
            intent.setPackage(getActivity().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
