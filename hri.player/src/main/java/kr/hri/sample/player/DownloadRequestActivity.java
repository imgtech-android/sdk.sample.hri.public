package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import org.andlib.helpers.Logger;

import java.net.URISyntaxException;

/**
 * 다운로드 요청 정보 수신 Activity
 * Scheme 수신은 Activity 만 가능
 * @author kimsanghwan
 * @since 2015.09.17.
 */
public class DownloadRequestActivity extends AppCompatActivity {

    // 다운로드 요청 URL Key
    private final static String URL_DOWNLOAD_REQ = "url_download_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        finish();

        new DownloadRequestAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * 다운로드 요청 저장
     * @param req    다운로드 요청
     */
    @SuppressLint("CommitPrefEdits")
    public static void setDownloadReq(Context c, String req) {

        SharedPreferences preferences
                = PreferenceManager.getDefaultSharedPreferences(c);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(URL_DOWNLOAD_REQ, req);
        editor.apply();
    }

    /**
     * 다운로드 요청 반환
     * @return  다운로드 요청 URL
     */
    public static String getDownloadReq(Context c) {

        SharedPreferences preferences
                = PreferenceManager.getDefaultSharedPreferences(c);

        return preferences.getString(URL_DOWNLOAD_REQ, "");
    }

    /**
     * 다운로드 처리 Activity 호출
     */
    private void startMainActivity() {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority(getString(R.string.host_download_working))
                ;

        Logger.d(uriBuilder.toString());

        // 해당 Player 로 Intent 전달
        try {
            Intent i = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
            i.addCategory(Intent.CATEGORY_BROWSABLE);
            i.setPackage(getPackageName());
            startActivity(i);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * 다운로드 인텐트를 DB 저장 처리 AsyncTask
     */
    private class DownloadRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            Intent intent = getIntent();
            setDownloadReq(getApplicationContext(), intent.getDataString());

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            startMainActivity();
        }
    }
}
