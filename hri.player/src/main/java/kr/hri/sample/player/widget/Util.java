package kr.hri.sample.player.widget;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Utility
 * Created by kimsanghwan on 2017. 9. 12..
 */
public class Util {

    private static Snackbar snackbar;

    /**
     * SnackBar 신규 생성 반환
     * @param view      Parent View
     * @param msg       표시 메시지
     * @param duration  표시 시간
     * @return  SnackBar
     */
    private static Snackbar getSnack(View view, String msg, int duration) {

        if (snackbar != null && snackbar.isShown()) {

            snackbar.dismiss();
            snackbar = null;
        }

        snackbar = Snackbar.make(view, msg, duration);

        return snackbar;
    }

    /**
     * SnackBar 신규 생성 반환
     * @param view      Parent View
     * @param msgID     표시 메시지 리소스 ID
     * @param duration  표시 시간
     * @return  SnackBar
     */
    private static Snackbar getSnack(View view, int msgID, int duration) {

        if (snackbar != null && snackbar.isShown()) {

            snackbar.dismiss();
            snackbar = null;
        }

        snackbar = Snackbar.make(view, msgID, duration);

        return snackbar;
    }

    /**
     * SnackBar 표시 여부
     * @return  true: SnackBar 표시, false: otherwise
     */
    public static boolean isSnack() {

        return (snackbar != null) && (snackbar.isShown());
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msg       표시 메시지
     * @param duration  표시 시간
     */
    public static void snack(View view, String msg, int duration) {

        getSnack(view, msg, duration).show();
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msgID     표시 메시지 리소스 ID
     * @param duration  표시 시간
     */
    public static void snack(View view, int msgID, int duration) {

        getSnack(view, msgID, duration).show();
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msg       표시 메시지
     * @param duration  표시 시간
     * @param actionText Action 문자열
     * @param listener  Action Listener
     */
    public static void snack(View view, String msg, int duration, String actionText, View.OnClickListener listener) {

        getSnack(view, msg, duration);
        snackbar.setAction(actionText, listener);
        snackbar.show();
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msgID     표시 메시지 리소스 ID
     * @param duration  표시 시간
     * @param actionText Action 문자열
     * @param listener  Action Listener
     */
    public static void snack(View view, int msgID, int duration, String actionText, View.OnClickListener listener) {

        getSnack(view, msgID, duration);
        snackbar.setAction(actionText, listener);
        snackbar.show();
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msg       표시 메시지
     * @param duration  표시 시간
     * @param actionTextID Action 문자열 리소스 ID
     * @param listener  Action Listener
     */
    public static void snack(View view, String msg, int duration, int actionTextID, View.OnClickListener listener) {

        getSnack(view, msg, duration);
        snackbar.setAction(actionTextID, listener);
        snackbar.show();
    }

    /**
     * SnackBar 메시지 표시
     * @param view      Parent View
     * @param msgID     표시 메시지 리소스 ID
     * @param duration  표시 시간
     * @param actionTextID Action 문자열 리소스 ID
     * @param listener  Action Listener
     */
    public static void snack(View view, int msgID, int duration, int actionTextID, View.OnClickListener listener) {

        getSnack(view, msgID, duration);
        snackbar.setAction(actionTextID, listener);
        snackbar.show();
    }

    /**
     * SnackBar 종료
     */
    public static void dismissSnack() {

        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
            snackbar = null;
        }
    }
}
