package kr.hri.sample.player;

/**
 * 다운로드 체크 리스너
 * Created by kimsanghwan on 2017. 8. 30..
 */
public interface IDownloadCheckListener {

    void onCheck(int mode);
}
