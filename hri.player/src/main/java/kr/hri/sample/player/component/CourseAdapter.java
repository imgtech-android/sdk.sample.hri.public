package kr.hri.sample.player.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kr.hri.sample.player.R;
import kr.imgtech.lib.zoneplayer.data.CourseInfoData;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.Extension;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.ItemTouchHelperExtension;
import kr.imgtech.lib.zoneplayer.util.StringUtil;
import kr.imgtech.lib.zoneplayer.widget.CircleImageView;

/**
 * 강좌 어댑터
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class CourseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<CourseInfoData> mCourseList;
    private ItemTouchHelperExtension mItemTouchHelperExtension;
    private CourseAdapterListener mListener;

    /**
     * 생성자
     * @param context   Context
     */
    public CourseAdapter(Context context) {

        mContext = context;
        mCourseList = new ArrayList<>();

        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.row_course_extension, parent, false);

        return new ItemSwipeWithActionWidthViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ItemBaseViewHolder baseViewHolder = (ItemBaseViewHolder) holder;
        baseViewHolder.bind(mCourseList.get(position));
        baseViewHolder.mLayoutCourse.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //Lib.toaster(mContext, "select: " + holder.getLayoutPosition());

                if (mListener != null) {

                    CourseInfoData data = mCourseList.get(position);
                    mListener.onItemSelectedListener(holder.getLayoutPosition(), data.isCert, data.intRemainTime);

                    /*if (! mCourseList.get(position).isCert) {
                        mListener.onItemSelectedListener(holder.getLayoutPosition(), mCourseList.get(position).intRemainTime);
                    } else {

                        if (mCourseList.get(position).intRemainTime > 0) {
                            mListener.onItemSelectedListener(holder.getLayoutPosition(), mCourseList.get(position).intRemainTime);
                        }
                    }*/
                }
            }
        });

        baseViewHolder.mLayoutCourse.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    if (mListener != null) {
                        mListener.onDeleteSelectedListener(holder.getLayoutPosition());
                    }

                    return true;
                }
            }
        );

        ItemSwipeWithActionWidthViewHolder viewHolder = (ItemSwipeWithActionWidthViewHolder) holder;
        viewHolder.mActionViewDelete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //Lib.toaster(mContext, "delete: " + holder.getLayoutPosition());

                if (mListener != null) {
                    mListener.onDeleteSelectedListener(holder.getLayoutPosition());
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return mCourseList.size();
    }

    /**
     * 선택 리스너 설정
     * @param listener  선택 리스너
     */
    public void setOnSelectedListener(CourseAdapterListener listener) {

        mListener = listener;
    }

    /**
     * 강좌 리스트 설정
     * @param list  강좌 리스트
     */
    public void setCourseList(List<CourseInfoData> list) {

        mCourseList.clear();
        mCourseList.addAll(list);
    }

    /**
     * 강좌 리스트 업데이트
     * @param list  강좌 리스트
     */
    public void updateList(List<CourseInfoData> list) {

        setCourseList(list);
        notifyDataSetChanged();
    }

    /**
     * 강좌 리스트 반환
     * @return  강좌 리스트
     */
    public ArrayList<CourseInfoData> getCourseList() {

        return (ArrayList<CourseInfoData>) mCourseList;
    }

    /**
     * 강좌 아이템 반환
     * @param position  반환 position
     * @return  강좌 아이템
     */
    public CourseInfoData getItem(int position) {

        return mCourseList.get(position);
    }

    /**
     * ItemTouchHelperExtension 설정
     * @param extension ItemTouchHelperExtension
     */
    public void setItemTouchHelperExtension(ItemTouchHelperExtension extension) {

        mItemTouchHelperExtension = extension;
    }

    /**
     * Item 추가
     * @param item	추가할 Item
     */
    public void addItem(CourseInfoData item) {

        mCourseList.add(item);
        notifyItemInserted(mCourseList.size() - 1);
    }

    /**
     * Index 설정하고 Item 추가
     * @param position	추가할 Index
     * @param item		추가할 Item
     * @return 추가된 Item Index
     */
    public int addItem(int position, CourseInfoData item) {

        if (position > mCourseList.size()) {
            position = mCourseList.size();
        }
        mCourseList.add(position, item);
        notifyItemInserted(position);

        return position;
    }

    /**
     * Item 삭제
     * @param item	삭제할 Item
     */
    public void removeItem(CourseInfoData item) {

        int position = mCourseList.indexOf(item);
        mCourseList.remove(position);

        notifyItemRemoved(position);
    }

    /**
     * 아이템 순서 변경
     * @param from  기존 position
     * @param to    신규 position
     */
    public void move(int from, int to) {

        Collections.swap(mCourseList, from, to);
        notifyItemMoved(from, to);
    }

    /**
     * 최종 Course Row ViewHolder
     */
    class ItemSwipeWithActionWidthViewHolder extends ItemBaseViewHolder implements Extension {

        View mActionViewDelete;

        /**
         * 생성자
         * @param itemView Extension View
         */
        ItemSwipeWithActionWidthViewHolder(View itemView) {

            super(itemView);
            mActionViewDelete = itemView.findViewById(R.id.iv_action_delete);
        }

        @Override
        public float getActionWidth() {
            return mLayoutExtension.getWidth();
        }
    }

    /**
     * 기본 Course Row ViewHolder
     */
    class ItemBaseViewHolder extends RecyclerView.ViewHolder {

        CircleImageView ivCourseThumb;
        TextView tvOnStudy;
        TextView tvTeacher;
        TextView tvCourseName;
        TextView tvEndTime;

        View mLayoutCourse;
        View mLayoutExtension;

        /**
         * 생성자
         * @param itemView  Course Row View
         */
        ItemBaseViewHolder(View itemView) {

            super(itemView);

            ivCourseThumb = (CircleImageView) itemView.findViewById(R.id.iv_course_thumb);
            tvOnStudy = (TextView) itemView.findViewById(R.id.tv_on_study);
            tvTeacher = (TextView) itemView.findViewById(R.id.tv_teacher);
            tvCourseName = (TextView) itemView.findViewById(R.id.tv_course_name);
            tvEndTime = (TextView) itemView.findViewById(R.id.tv_end_time);

            mLayoutCourse = itemView.findViewById(R.id.row_course);
            mLayoutExtension = itemView.findViewById(R.id.row_action);
        }

        void bind (CourseInfoData data) {

            if (data.isCert) {

                // 강의 기간 유효
                if (data.intRemainTime > 0) {

                    if (data.courseImageResID > 0) {

                        ivCourseThumb.setImageResource(data.courseImageResID);
                    } else {

                /*InputStream is;

                try {
                    URL url = new URL("file://" + data.courseImagePath);
                    is = url.openStream();
                } catch (IOException e) {
                    e.printStackTrace();
                    is = null;
                }

                if (is != null) {
                    ivCourseThumb.setImageBitmap(BitmapFactory.decodeStream(is));
                }*/

                        Picasso.with(mContext)
                                .load("file://" + data.courseImagePath)
                                .placeholder(R.drawable.ic_account_circle)
                                .into(ivCourseThumb);
                    }

                    // 수강중 설정
                    tvOnStudy.setText((data.onStudy == 1) ? R.string.on_study : R.string.yet_study);
                    tvOnStudy.setBackgroundResource((data.onStudy == 1) ? R.color.indigo_500 : R.color.red_500);

                    tvTeacher.setText(data.teacherName);
                    tvCourseName.setText(data.courseName);

                    if (StringUtil.isNotBlank(data.endTime) && (StringUtil.isNotBlank(data.remainTime))) {

                        tvEndTime.setText(
                                mContext.getString(R.string.end_time) +  ": " + data.endTime
                                        + " " + mContext.getString(R.string.remain_time) + ": " + data.remainTime + mContext.getString(R.string.day));
                    } else {

                        tvEndTime.setText("-");
                    }

                    // 강의 기간 만료
                } else {

                    if (data.courseImageResID > 0) {

                        ivCourseThumb.setImageResource(data.courseImageResID);
                    } else {

                        Picasso.with(mContext)
                                .load("file://" + data.courseImagePath)
                                .placeholder(R.drawable.ic_account_circle)
                                .into(ivCourseThumb);
                    }

                    setGrayScale(ivCourseThumb);
                    tvOnStudy.setBackgroundColor(
                            ResourcesCompat.getColor(mContext.getResources(), R.color.font_light, mContext.getTheme()));
                    tvCourseName.setTextColor(
                            ResourcesCompat.getColor(mContext.getResources(), R.color.font_light, mContext.getTheme()));

                    tvOnStudy.setText(mContext.getString(R.string.expired));
                    tvTeacher.setText(data.teacherName);
                    tvCourseName.setText(data.courseName);
                    if (StringUtil.isNotBlank(data.endTime) && (StringUtil.isNotBlank(data.remainTime))) {

                        tvEndTime.setText(
                                mContext.getString(R.string.end_time) +  ": " + data.endTime
                                        + " " + mContext.getString(R.string.remain_time) + ": " + data.remainTime + mContext.getString(R.string.day));
                    } else {

                        tvEndTime.setText("-");
                    }
                }

            } else {

                if (data.courseImageResID > 0) {

                    ivCourseThumb.setImageResource(data.courseImageResID);
                } else {

                    Picasso.with(mContext)
                            .load("file://" + data.courseImagePath)
                            .placeholder(R.drawable.ic_account_circle)
                            .into(ivCourseThumb);
                }

                // 수강중 설정
                tvOnStudy.setText((data.onStudy == 1) ? R.string.on_study : R.string.yet_study);
                tvOnStudy.setBackgroundResource((data.onStudy == 1) ? R.color.indigo_500 : R.color.red_500);

                tvTeacher.setText(data.teacherName);
                tvCourseName.setText(data.courseName);

                if (StringUtil.isNotBlank(data.endTime) && (StringUtil.isNotBlank(data.remainTime))) {

                    tvEndTime.setText(mContext.getString(R.string.no_expired));
                }
            }

        }
    }

    private void setGrayScale(ImageView view) {

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);                        //0이면 gray-scale
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        view.setColorFilter(cf);
    }

    /**
     * 어댑터 아이템 선택 리스너
     */
    public interface CourseAdapterListener {

        /**
         * 아이템 선택 반환
         * @param position  선택 아이템 position
         */
        void onItemSelectedListener(int position, boolean isCert, int remainTime);

        /**
         * 삭제 선택 반환
         * @param position  삭제 아이템 position
         */
        void onDeleteSelectedListener(int position);
    }
}
