package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import org.andlib.helpers.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import kr.hri.sample.player.widget.DownloadDialog;
import kr.hri.sample.player.widget.Util;
import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.data.IntentDataParser;
import kr.imgtech.lib.zoneplayer.data.IntentDataParserListener;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadInterface;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadService4;
import kr.imgtech.lib.zoneplayer.interfaces.ActiveDownload;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadItemListener;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadService;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadServiceCallback;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadReqData;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.Lib;
import kr.imgtech.lib.zoneplayer.util.StringUtil;
import kr.imgtech.lib.zoneplayer.util.WeakHandler;

/**
 * 다운로드 Manager
 * Created by kimsanghwan on 2017.09.07
 */
public class DownloadManager6 extends Fragment implements IBackPressedListener,
        BaseInterface,
        BaseDialogListener,
        IntentDataParserListener,
        IntentDataDefine,
        DownloadInterface {

    /*
     * Handler Message Constants 정의
     */
    private final static int MSG_UPDATE_PROGRESS        = 1;	// 다운로드 진행 메시지
    private final static int MSG_NOTIFY_STATUS          = 2;	// 다운로드 상태 알림 메시지
    private final static int MSG_FILE_INFORMATION       = 3;	// 콘텐츠 파일 정보 획득 메시지

    private final static int MSG_START_DOWNLOAD_PROGRESS= 4;	// 다운로드 프로그레스 일괄 시작
    private final static int MSG_END_NOTIFICATION       = 5;	// Notification 해제 메시지

    private final static int MSG_RESUME_DOWNLOAD        = 6;    // 다운로드 재개 시작
    private final static int MSG_STOP_DOWNLOAD          = 7;    // 다운로드 서비스 정지
    private final static int MSG_NEXT_DOWNLOAD          = 8;    // 다음 다운로드 진행
    private final static int MSG_ADD_ROW_VIEW           = 9;    // Row View 추가

    private final static int MSG_RELEASE_SERVICE_GUARD  = 10;   // 다운로드 서비스 보호 지연 메시지

    /*
     * Message Data Key
     */
    private final static String KEY_MSG_STATUS  = "status";		    // 다운로드 상태
    private final static String KEY_MSG_MESSAGE = "message";	    // 전달 메시지
    private final static String KEY_CERT_CODE   = "cert-code";      // 인증 결과 코드
    private final static String KEY_CERT_MESSAGE= "cert-message";   // 인증 결과 메시지

    private final static long AVAIL_USABLE_SPACE = (500L * 1024L * 1024L);  // 다운로드 가능 유휴 공간 (500MB)
    private final static int TIME_UPDATE_PROGRESS = 500;	                // Progress 업데이트 주기 0.5초
    private final static int TIME_SERVICE_GUARD = 4000;                     // 다운로드 서비스 보호 시간

    @SuppressLint("StaticFieldLeak")
    private static DownloadManager6 instance;
    private Intent mIntent;

    private ContentsDatabase2 mDB;
    private LayoutInflater mInflater;

    @SuppressLint("StaticFieldLeak")
    private static LinearLayout mLayoutProgress;	// 다운로드 아이템 목록 표출 ViewGroup

    private static boolean mUpdateProgress;	        // TIME_UPDATE_PROGRESS 마다 동작하기 위한 판단 변수
    private boolean mDownloadServiceRequest;        // download service 명령 요청 후 DownloadService 로부터의 응답 전달 여부

    private static ArrayList<ZoneDownloadReqData> mListDownloadReq = new ArrayList<>();
    private static ArrayList<DownloadItemListener> mListListener = new ArrayList<>();

    public static ActiveDownload mActiveDownload;	// 활성 다운로드

    // 다운로드 서비스 Interface
    private static IDownloadService mService;   // DownloadService4 instance
    private static boolean mRegistered;		    // 서비스에 등록되었는지 여부

    // 다운로드 프로그레스 전체 Handler
    private static Handler handler;
    private boolean mServiceGuard = false;      // 다운로드 서비스 연속 재개 및 정지 보호 변수

    // 네트워크 상태 리시버
    private NetworkStateReceiver mNetworkStateReceiver;

    // 미디어 마운트 상태 리시버
    private MediaMountReceiver mMediaMountReceiver;
    private static boolean mMediaMountError;                // 외부 저장소 탈거 여부

    // Notification 관련 변수
    private static Notification mNotification;              // Notification
    private static NotificationManager mNotificationManager;// Notification Manager
    private static RemoteViews mRemoteView;                 // 다운로드 상태바 표시 RemoteViews
    private boolean mUseDownloadNotification;               // 다운로드 상태바 표시 여부
    private boolean mBlockNotification;                     // stopDownload 에 따라 delay 전달되는 Notification Update Block 변수

    /**
     * IDownloadServiceCallback 구현
     */
    private static IDownloadServiceCallback mCallback = new IDownloadServiceCallback.Stub() {

        @Override
        public void valueChanged(int downloadId, int downloadResult) throws RemoteException {

            // 활성 다운로드 초기화
            mActiveDownload = null;

            // 무조건 외장 SD 카드 확인
            if (! ContentFileManager.getInstance(IMGApplication.context).availExtSDCard()) {
                ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
                ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);
            }

            // 다운로드 상태가 정지이면 이후 진행하지 않음
            if (downloadResult == RESULT_STOP) {
                Logger.d("paused download");
                return;
            }

            // 미디어 마운트 에러이면 이후 진행하지 않음
            if (mMediaMountError) {
                //handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);
                handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);
                return;
            }

            // 1. download-id 로 list index 조회
            int itemIndex = instance.getIndexByDownloadID(downloadId);

            // 2. 현재 완료된 다운로드가 다운로드 성공이면 내용 실행
            if (downloadResult == RESULT_SUCCESS) {

                // 다운로드 요청 DB 에서 삭제
                int ret = instance.mDB.deleteDownloadReq(downloadId);

                if (ret > 0) {

                    // IDownloadServiceCallback.Stub() 내 다운로드 결과가 성공인 경우
                    // DownloadState Broadcast 전송. 다운로드 완료
                    instance.sendDownloadStateBroadcast(itemIndex, 2);
                    // 현재 View 및 ArrayList 에서 삭제
                    instance.removeDownloadItem(itemIndex);
                }

            } else {

                // 다운로드 성공이 아니면 기존 다운로드 건이 그대로 남아있으므로 다음 건부터 찾기 위해 ++
                itemIndex ++;
            }

            // 3. start Index 로 이후 다운로드 요청 건이 있는지 확인
            // 다운로드 완료
            if ((itemIndex) > (mListDownloadReq.size() - 1)){

                Logger.d("start index >= mListDownloadReq.size()");
                handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);
                return;
            }

            // 4. 현재 전달받은 완료된 다운로드 id 이후 아이템 중 다운로드할 아이템 조회
            int nextDownloadIndex = -1;
            for (int i = itemIndex; i < mListDownloadReq.size(); i++) {

                // 다운로드 성공이 아닌 아이템 조회
                if (mListDownloadReq.get(i).downloadResult < RESULT_SUCCESS) {
                    nextDownloadIndex = i;
                    break;
                }
            }

            // 5. 다음 다운로드할 아이템이 있으면 실행
            if (nextDownloadIndex > -1) {

                Logger.d("next download index: " + nextDownloadIndex);

                handler.removeMessages(MSG_RESUME_DOWNLOAD);

                // Message 전달
                Message message = handler.obtainMessage(MSG_NEXT_DOWNLOAD, nextDownloadIndex, -1);
                handler.sendMessageDelayed(message, 1000);	// Message 전달

            } else {

                handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);
            }

        }
    };

    /**
     * DownloadState Broadcast 전송
     * @param index 다운로드 인덱스
     * @param state 다운로드 상태. 1: 시작, 2: 완료
     */
    private void sendDownloadStateBroadcast(int index, int state) {

        ZoneDownloadReqData data = mListDownloadReq.get(index);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("index", index);
            jsonObject.put(LECTURE_NAME, data.lectureName);
            jsonObject.put(FILE_NAME, data.fileName);
            jsonObject.put("state", state);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(DownloadStateReceiver.INTENT_DOWNLOAD_STATE);
        intent.putExtra("data", jsonObject.toString());
        IMGApplication.context.sendBroadcast(intent);
    }

    // 다운로드 서비스 Connection
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            if (service != null) {

                mService = IDownloadService.Stub.asInterface(service);

                // 서비스 인스턴스 여부 확인
                if (mService != null) {
                    try {
                        mRegistered = mService.registerCallback(mCallback);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    if (mActiveDownload != null) {

                        DownloadManager6.mUpdateProgress = false;

                        // 서비스 시작
                        try {
                            mService.setActiveDownload(mActiveDownload);
                            mService.resumeDownload();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        // mNotification 시작
                        startNotification(mActiveDownload.DOWNLOAD_REQ_DATA.lectureName);
                    }
                }

                // 서비스 인스턴스 확인 및 서비스 콜백에 정상 등록 여부 확인
                /*if (mRegistered) {
                    handler.sendEmptyMessage(MSG_START_DOWNLOAD_PROGRESS);
                } else {
                    Lib.toaster(IMGApplication.context, "다운로드 서비스가 정상 실행되지 않았습니다.");
                }*/
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            if (mService != null) {
                try {
                    mService.unregisterCallback(mCallback);
                    mService = null;
                    mRegistered = false;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

    };

    /**
     * 생성자
     */
    public DownloadManager6() {

        super();
    }

    /**
     * DownloadFragment instance 반환
     * @return  DownloadFragment
     */
    public static DownloadManager6 getInstance() {

        if (instance == null) {
            instance = new DownloadManager6();
        }

        instance.mIntent = null;

        if (handler == null) {
            handler = new DownloadProgressHandler(instance);
        }

        return instance;
    }

    /**
     * DownloadFragment instance 반환
     * @param bundle    Download Intent Bundle
     * @return  DownloadFragment
     */
    public static DownloadManager6 getInstance(Bundle bundle) {

        instance = getInstance();

        if (bundle != null) {

            instance.mIntent = bundle.getParcelable(MainActivity.KEY_INTENT);
        }

        return instance;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup container, Bundle savedInstanceState) {


        mDB = ContentsDatabase2.getInstance(IMGApplication.context);

        mInflater = inf;
        return inf.inflate(R.layout.fragment_download, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) getActivity()).getFloatingActionButton().setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                dialogRemoveAllDownload();
            }
        });

        initList(view);

        startMediaMountReceiver();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // 2. 현재 리스트 상태 저장
        saveLastDownloadReqStatus();

        // 3. 데이터 초기화
        resetData();

        stopMediaMountReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();

        // 무조건 외장 SD 카드 확인
        if (! ContentFileManager.getInstance(IMGApplication.context).availExtSDCard()) {
            ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
            ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);
        }

        startDownloadProgress();
    }

    @Override
    public void onPause() {
        super.onPause();

        // 무조건 외장 SD 카드 확인
        if (! ContentFileManager.getInstance(IMGApplication.context).availExtSDCard()) {
            ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
            ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);
        }
    }

    @Override
    public void onBackPressed() {

        ((MainActivity)getActivity()).finishApplicationProcess();
    }

    @Override
    public void onDialog(int state, int code, int result) {

        switch (state) {

            // 다운로드 전체 취소
            case DIALOG_DOWNLOAD_DELETE_ALL:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {
                    setRemoveAllDownload();
                }
                break;

            // 다운로드 아이템 취소
            case DIALOG_DOWNLOAD_CANCEL_ITEM:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    try {
                        mService.cancelDownload(code);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;

            // 최초 화면 생성 시 네트워크 경고
            case DIALOG_NETWORK_WARNING:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    // 3. 다운로드 서비스 바인드
                    startServiceBind();
                }
                break;

            // 활성 다운로드 삭제 확인 메시지
            case DIALOG_CONFIRM_DELETE_ACTIVE_DOWNLOAD:

                switch (result) {

                    case DownloadDialog.DIALOG_CHOICE_POSITIVE: // 삭제

                        // 다운로드 정지
                        handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);

                        ZoneDownloadReqData reqData = mListDownloadReq.get(code);

                        // 파일 삭제
                        if (reqData.downloadResult != RESULT_SUCCESS) {
                            ContentFileManager.getInstance(IMGApplication.context).deleteFile(
                                    mActiveDownload.DOWNLOAD_REQ_DATA.filePath);
                        }

                        // 리스트 아이템 삭제
                        removeDownloadItem(code);

                        // 활성 다운로드 삭제
                        mActiveDownload = null;
                        break;
                }
                break;

            // 활성 다운로드 포함 전체 삭제 확인 메시지
            case DIALOG_CONFIRM_DELETE_ALL:

                switch (result) {

                    case DownloadDialog.DIALOG_CHOICE_POSITIVE: // 삭제

                        // 다운로드 정지
                        handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);

                        // 전체 삭제
                        setRemoveAllDownload();

                        // 활성 다운로드 삭제
                        mActiveDownload = null;
                        break;
                }
                break;

            // 활성 다운로드 삭제 확인 메시지
            case DIALOG_CONFIRM_DELETE_ITEM:

                switch (result) {

                    case DownloadDialog.DIALOG_CHOICE_POSITIVE: // 삭제

                        //Lib.toaster(IMGApplication.context, "download-id: " + downloadID);

                        // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
                        int index = getIndexByDownloadID(code);
                        ZoneDownloadReqData reqData = mListDownloadReq.get(index);

                        // 활성 다운로드 있음
                        if (mActiveDownload != null) {

                            // 활성 다운로드 삭제 시도
                            if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == code) {
                                dialogConfirmDeleteDownload(index);
                            } else {

                                // 파일경로가 존재하면 삭제
                                if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                                    ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                                }

                                removeDownloadItem(index);
                            }

                        } else {

                            // 파일경로가 존재하면 삭제
                            if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                                ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                            }

                            removeDownloadItem(index);
                        }
                        break;
                }
                break;

            // 네트워크 없음
            case DIALOG_NO_NETWORK:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    // 네트워크 미연결
                    if (! Lib.isNetworkAvailable(IMGApplication.context)) {

                        // 네트워크 없음 알림
                        dialogNoNetwork(code);
                        return;
                    }

                    Message message = handler.obtainMessage(MSG_RESUME_DOWNLOAD, code, -1);
                    handler.sendMessageDelayed(message, 1000);	// Message 전달
                }
                break;
        }
    }

    @Override
    public void onParseFail(final int errorCode) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                Lib.toaster(IMGApplication.context, "파싱 오류 " + errorCode, Toast.LENGTH_LONG);
            }
        });

    }

    @Override
    public void onParseMessage(final int error, final String message) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                Lib.toaster(IMGApplication.context, "파싱 오류 " + error + " / " + message, Toast.LENGTH_LONG);
            }
        });
    }

    /**
     * DownloadService 시작 및 바인드
     * Fragment 가 생성되면 반드시 호출 요망
     */
    private void startServiceBind() {

        if ((mService == null) && (!mRegistered)) {

            IMGApplication.context.startService(new Intent(IMGApplication.context, DownloadService4.class));
            IMGApplication.context.bindService(new Intent(IMGApplication.context, DownloadService4.class),
                    mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    /**
     * DownloadService 언바인드 및 정지
     * Fragment 가 해제되면 반드시 호출 요망
     */
    private void unbindService() {

        if ((mService != null) && (mRegistered)) {

            IMGApplication.context.unbindService(mConnection);
            IMGApplication.context.stopService(new Intent(IMGApplication.context, DownloadService4.class));

            mService = null;
            mRegistered = false;
        }
    }

    public void startDownloadManager() {

        mDB = ContentsDatabase2.getInstance(IMGApplication.context);
        mInflater = (LayoutInflater) IMGApplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        startMediaMountReceiver();

        restoreList();
        startDownloadProgress();
    }

    /**
     * 네트워크 상태 변화 수신 리시버 정지
     */
    private void stopNetworkStateReceiver() {

        if (IMGApplication.context == null) {
            return;
        }

        if (mNetworkStateReceiver != null) {
            IMGApplication.context.unregisterReceiver(mNetworkStateReceiver);
            mNetworkStateReceiver = null;
        }
    }

    /**
     * 미디어 마운트 리시버 시작
     */
    private void startMediaMountReceiver() {

        if (IMGApplication.context == null) {
            return;
        }

        // 외부 저장소 사용 상태가 아님
        if (!ConfigurationManager.getUseExtSDCard(IMGApplication.context)) {
            return;
        }

        if (mMediaMountReceiver != null) {
            return;
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        //filter.addAction(Intent.ACTION_UMS_CONNECTED);
        //filter.addAction(Intent.ACTION_UMS_DISCONNECTED);
        //filter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        //filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        filter.addDataScheme("file");

        mMediaMountReceiver = new MediaMountReceiver();
        IMGApplication.context.registerReceiver(mMediaMountReceiver, filter);
    }

    /**
     * 미디어 마운트 리시버 정지
     */
    private void stopMediaMountReceiver() {

        if (IMGApplication.context == null) {
            return;
        }

        // 외부 저장소 사용 상태가 아님
        if (!ConfigurationManager.getUseExtSDCard(IMGApplication.context)) {
            return;
        }

        if (mMediaMountReceiver != null) {
            IMGApplication.context.unregisterReceiver(mMediaMountReceiver);
            mMediaMountReceiver = null;
        }
    }

    /**
     * 전달받은 다운로드 Intent 처리 시작
     * Intent 가 있는 경우에만 진행
     */
    private void startDownloadProgress() {

        if (mIntent != null) {

            new DataProcessingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            //new TestTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    /**
     * Intent 데이터 초기화
     * @return  파싱 완료된 ZonePlayerData
     */
    private ArrayList<ZoneDownloadReqData> initData() {

        if (mIntent != null) {

            IntentDataParser parser = new IntentDataParser(IMGApplication.context, mIntent, this);	// IntentDataParser 생성 및 Intent 전달

            return (ArrayList<ZoneDownloadReqData>) parser.parseIntentData();	// 파싱 및 ZonePlayerData 생성
        } else {

            return  null;
        }
    }

    /**
     * List 초기화
     */
    private void initList(View view) {

        mLayoutProgress = (LinearLayout) view.findViewById(R.id.layout_download);

        restoreList();
    }

    /**
     * 다운로드 목록을 DB와 비교해서 복원
     */
    private void restoreList() {

        ArrayList<ZoneDownloadReqData> dbList = mDB.getDownloadReqInfo();

        if (mLayoutProgress != null) {
            mLayoutProgress.removeAllViews();
        }

        if ((mListDownloadReq.size() == 0) && (dbList.size() > 0)) {

            for (ZoneDownloadReqData reqData : dbList) {

                // 다운로드 아이템 리스너 생성
                DownloadItemListener listener = new DownloadItemListener();

                // root-view 에 추가
                addDownloadItem(reqData, listener, true);
            }
        } else {

            for (int i = 0; i < mListDownloadReq.size(); i++) {

                ZoneDownloadReqData reqData = mListDownloadReq.get(i);

                DownloadItemListener listener;
                if (mListListener.size() > i) {
                    listener = mListListener.get(i);
                } else {
                    listener = new DownloadItemListener();
                }

                addDownloadItem(reqData, listener, false);
            }
        }
    }

    /**
     * 데이터 초기화
     */
    private void resetData() {

        mDownloadServiceRequest = false;
        mMediaMountError = false;
    }

    /**
     * double 데이터를 소수점 x자리 문자열로 변환
     * @param size	소수점 자리 수
     * @return	변환된 문자열
     */
    private String getFormatRateDouble(double size) {
        return String.format(java.util.Locale.US, "%.1f", size);
    }

    /**
     * Time 을 날짜 형식 문자로 반환
     * @param date	Time (초 단위)
     * @return 날짜 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDate(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return String.format("%04d.%02d.%02d", year, mon + 1, day);
    }

    /**
     * 다운로드 요청 데이터 존재 여부 반환
     * @return  true: 다운로드 요청 데이터 반환. false: 다운로드 요청 데이터 없음
     */
    private boolean existDownloadReq() {

        ArrayList<ZoneDownloadReqData> list = mDB.getDownloadReqInfo();

        return (list != null) && (list.size() > 0);
    }

    /**
     * 최종 다운로드 상태 저장
     */
    private void saveLastDownloadReqStatus() {

        // 1. 다운로드 완료된 아이템 삭제
        for (int i = 0; i < mListDownloadReq.size(); i++) {
            if (mListDownloadReq.get(i).downloadResult == RESULT_SUCCESS) {

                removeDownloadItem(i);
            }
        }

        // 2. 기존 다운로드 DB 테이블 삭제
        mDB.deleteAllDownloadReq();

        // 현재 리스트에 있는 다운로드 데이터를 DB에 재저장
        // index 및 order 재설정
        for (int i = 0; i < mListDownloadReq.size(); i++) {

            ZoneDownloadReqData data = mListDownloadReq.get(i);

            // 다운로드 시간 및 다운로드 ID 유지, 다운로드 순서만 다시 저장.
            mDB.addDownloadReqInfo(data, data.downloadID, i, data.downloadTime);
        }
    }

    /**
     * 다운로드 ID 로 현재 리스트에서 인덱스 검색
     * @param downloadID 검색할 다운로드 ID
     * @return 인덱스
     */
    private int getIndexByDownloadID(int downloadID) {

        int ret = -1;

        for (int i = 0; i < mListDownloadReq.size(); i++) {
            if (downloadID == mListDownloadReq.get(i).downloadID) {
                ret = i;
                break;
            }
        }

        return ret;
    }

    /**
     * 다운로드 아이템 추가
     * @param reqData   ZoneDownloadReqData
     * @param listener  DownloadItemListener
     * @param add 다운로드 array list 추가 여부
     */
    @SuppressLint({"InflateParams", "SetTextI18n"})
    private synchronized void addDownloadItem(ZoneDownloadReqData reqData, DownloadItemListener listener, boolean add) {

        // row 를 이루는 view-group inflate
        View view = mInflater.inflate(R.layout.row_download6, null);

        // child View 초기화
        TextView tvCourseName = (TextView) view.findViewById(R.id.tv_course_name);
        TextView tvLectureName = (TextView) view.findViewById(R.id.tv_lecture_seq_name);
        ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
        //ImageView ivRemove= (ImageView) view.findViewById(R.id.iv_action_delete);

        TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
        TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
        ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
        pbProgress.setMax(100);

        // 강좌명
        tvCourseName.setText(reqData.courseName);

        // 강의명
        tvLectureName.setText(reqData.lectureName);

        // 다운로드 정지/재개 view 클릭 리스너 설정
        ivPauseResume.setOnClickListener(
                new DownloadStopResumeListener(reqData.downloadID, ivPauseResume));

        // 아이템 삭제 리스너 설정
        /*ivRemove.setOnClickListener(
                new RemoveItemListener(reqData.downloadID));*/

        view.setOnLongClickListener(new RemoveItemListener(reqData.downloadID));

        switch (reqData.downloadResult) {

            case RESULT_FAIL:
            case RESULT_STOP:
            case RESULT_PROCESS:

                // 파일 크기 설정
                int percent;
                String currSize;

                // 다운로드 rate 계산
                percent = (int) (((double) reqData.downloadSize / (double) reqData.fileSize) * 100);

                // 받은 사이즈 / 총 사이즈 문자열 구성
                if (reqData.fileSize > 0) {
                    currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB / "
                            + getFormatRateDouble((double) reqData.fileSize / (double) 1024000)
                            + " MB";


                    tvProgress.setText(currSize);
                }

                // 다운로드 실패 - 실패 메시지 표출
                if (reqData.downloadResult == RESULT_FAIL) {
                    tvRate.setText(reqData.downloadMsg);

                    // 다운로드 정지 - rate % 표출
                } else {
                    tvRate.setText(percent + " %");
                }

                pbProgress.setProgress(percent);
                break;

            case RESULT_SUCCESS:

                tvProgress.setText(
                        String.format("%s MB", getFormatRateDouble(((double) reqData.fileSize / (double) 1024000))));

                tvRate.setVisibility(View.GONE);
                pbProgress.setVisibility(View.GONE);

                ivPauseResume.setVisibility(View.GONE);
                break;

        }

        // 활성 다운로드 존재
        if (mActiveDownload != null) {
            if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == reqData.downloadID) {

                ivPauseResume.setImageResource(R.drawable.ic_pause_circle_outline);

            } else {

                ivPauseResume.setImageResource(R.drawable.ic_file_download);

            }

            // 활성 다운로드 없음 - 모든 아이템이 다운로드 버튼으로만 표출
        } else {

            ivPauseResume.setImageResource(R.drawable.ic_file_download);
        }

        if (add) {
            mListDownloadReq.add(reqData);
            mListListener.add(listener);
        }

        //view.setSwipeGestureListener(defaultSwipeGestureListener);

        listener.setView(view);

        // List 에 row 추가
        Message message = handler.obtainMessage(MSG_ADD_ROW_VIEW, 0, 0, view);
        handler.sendMessage(message);
    }

    @SuppressLint("InflateParams")
    private synchronized void removeDownloadItem(int index) {

        int downloadID = mListDownloadReq.get(index).downloadID;

        mListDownloadReq.remove(index);
        mListListener.remove(index);

        if (mLayoutProgress != null) {
            mLayoutProgress.removeViewAt(index);
        }

        mDB.deleteDownloadReq(downloadID);
    }

    /**
     * 다운로드 진행 단계 수치 업데이트
     * @param view		업데이트할 rowView
     */
    @SuppressLint("SetTextI18n")
    private void progressItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // 다운로드 성공 및 실패 시 처리하지 않음
            if (reqData.downloadResult == RESULT_SUCCESS
                    || reqData.downloadResult == RESULT_FAIL) {
                return;
            }

            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
            TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
            ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);

            // 다운로드 rate 계산
            int percent = (int) (((double) reqData.downloadSize / (double)reqData.fileSize) * 100);

            String currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                    + " MB / "
                    + getFormatRateDouble((double) reqData.fileSize / (double) 1024000)
                    + " MB";

            tvProgress.setText(currSize);
            tvRate.setText(percent + " %");

            pbProgress.setProgress(percent);

            updateProgressNotification(currSize, percent);
        }
    }

    /**
     * 다운로드 아이템 상태 설정
     * 다운로드 완료 / 실패 / 취소의 3가지 상태 설정
     * @param view		설정을 표출할 rowView
     * @param index		설정할 다운로드 아이템의 인덱스
     */
    @SuppressLint("SetTextI18n")
    private void statusItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            // 다운로드 요청 데이터 획득
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            if (!mMediaMountError) {
                // 외부 저장소 사용
                if (ConfigurationManager.getUseExtSDCard(IMGApplication.context)) {
                    reqData.downloadLocation = LOCATION_EXT1;
                    Logger.d("외부 저장소 사용");
                    // 내부 저장소 사용
                } else {
                    reqData.downloadLocation = LOCATION_INNER;
                    Logger.d("내부 저장소 사용");
                }
            }

            Logger.d("index: " + index + ", result: " + reqData.downloadResult);

            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
            TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
            ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);

            ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);

            switch (reqData.downloadResult) {

                // 다운로드 성공
                case RESULT_SUCCESS:

                    tvProgress.setText(
                            String.format("%s MB", getFormatRateDouble(((double) reqData.fileSize / (double) 1024000))));

                    tvRate.setVisibility(View.GONE);
                    pbProgress.setVisibility(View.GONE);

                    ivPauseResume.setVisibility(View.GONE);
                    break;

                // 다운로드 정지
                case RESULT_STOP:

                    // 다운로드 rate 계산
                    int percent = (int) (((double) reqData.downloadSize / (double) reqData.fileSize) * 100);

                    String currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB / "
                            + getFormatRateDouble((double) reqData.fileSize / (double) 1024000)
                            + " MB";

                    tvProgress.setText(currSize);
                    tvRate.setText(percent + " %");

                    pbProgress.setProgress(percent);

                    ivPauseResume.setImageResource(R.drawable.ic_file_download);
                    break;

                // 다운로드 실패
                case RESULT_FAIL:

                    // 다운로드 rate 계산
                    percent = (int) (((double) reqData.downloadSize / (double) reqData.fileSize) * 100);

                    currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB / "
                            + getFormatRateDouble((double) reqData.fileSize / (double) 1024000)
                            + " MB";

                    tvProgress.setText(currSize);
                    pbProgress.setProgress(percent);

                    ivPauseResume.setImageResource(R.drawable.ic_file_download);
                    tvRate.setText(reqData.downloadMsg);
                    break;

                // 다운로드 진행 중
                case RESULT_PROCESS:

                    ivPauseResume.setImageResource(R.drawable.ic_pause_circle_outline);
                    break;
            }
        }
    }

    private void infoItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            // 다운로드 요청 데이터 획득
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // item-view 의 상세 view 획득
            ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);

            switch (reqData.downloadResult) {

                case RESULT_FAIL:
                case RESULT_STOP:
                    ivPauseResume.setImageResource(R.drawable.ic_file_download);
                    break;

                // 다운로드 진행 중
                case RESULT_PROCESS:
                    ivPauseResume.setImageResource(R.drawable.ic_pause_circle_outline);
                    break;

                case RESULT_SUCCESS:

                    tvProgress.setText(
                            String.format("%s MB", getFormatRateDouble(((double) reqData.fileSize / (double) 1024000))));
                    break;
            }
        }
    }

    /**
     * Notification 표출
     * Handler 에 의해 실행되므로 별도의 Thread 필요 없음
     * @param title	타이틀
     */
    private void startNotification(String title) {

        mUseDownloadNotification = ConfigurationManager.getUseDownloadNotification(IMGApplication.context);

        if (! mUseDownloadNotification) {
            return;
        }

        mBlockNotification = false;

        mNotificationManager = (NotificationManager)
                IMGApplication.context.getSystemService(Context.NOTIFICATION_SERVICE);

        // 실행 Intent 생성
        Intent intent = makeDownloadProgressIntent();
        PendingIntent contentIntent = PendingIntent.getActivity(IMGApplication.context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 종료 Intent 생성
        /*Intent closeIntent = new Intent(ACTION_RELEASE_NOTIFICATION);
        PendingIntent closePendingIntent = PendingIntent.getBroadcast(IMGApplication.context, 0,
                closeIntent, 0);*/

        // RemoteViews 생성 및 초기화
        mRemoteView = new RemoteViews(IMGApplication.context.getPackageName()
                , R.layout.notification_progress);

        mRemoteView.setTextViewText(R.id.tv_title, title);
        mRemoteView.setTextViewText(R.id.tv_size, "");
        mRemoteView.setTextViewText(R.id.tv_percent, "0%");
        mRemoteView.setProgressBar(R.id.pb_download_item, 100, 0, true);
        //notificationProgressView.setOnClickPendingIntent(R.id.btn_close, closePendingIntent);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(IMGApplication.context);

        notificationBuilder
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_move_to_inbox_white)
                .setContentIntent(contentIntent)
                .setCustomContentView(mRemoteView)
//        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        ;

        mNotification = notificationBuilder.build();

        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }

    /**
     * 다운로드 아이템의 다운로드 프로그레스 업데이트
     * @param size	현재까지 다운로드된 파일 사이즈
     * @param progress	업데이트할 프로그레스
     */
    private void updateProgressNotification(String size, int progress) {

        if (! mUseDownloadNotification) {
            return;
        }

        if (mBlockNotification) {
            return;
        }

        if ((mNotificationManager != null) && (mRemoteView != null)) {

            mRemoteView.setTextViewText(R.id.tv_size, size);
            mRemoteView.setTextViewText(R.id.tv_percent, progress + "%");
            mRemoteView.setProgressBar(R.id.pb_download_item, 100, progress, false);

            mNotificationManager.notify(NOTIFICATION_ID, mNotification);
        }
    }

    /**
     * Notification 초기화 및 해제
     */
    private void releaseNotification() {

        if (! mUseDownloadNotification) {
            return;
        }

        if (mBlockNotification) {
            return;
        }

        if (mNotificationManager != null) {
            mNotificationManager.cancel(NOTIFICATION_ID);
        }

        mUpdateProgress = false;
    }

    /**
     * data = blank 를 넣은 Download Progress Intent 생성
     * @return	 Download Progress Intent
     */
    private Intent makeDownloadProgressIntent() {

        Intent intent = new Intent(IMGApplication.context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        return intent;
    }

    /**
     * 설정된 저장 매체의 공간이 유효한 공간인지 여부 반환
     * @return  true 유효 공간. false 공간 부족
     */
    private boolean isUsableSpace() {

        long usable = ContentFileManager.getInstance(IMGApplication.context).getCurrentUsableSpace();

        Logger.d("usable-space: " + usable);

        // 1024000000f
        return (usable >= AVAIL_USABLE_SPACE);
    }

    private void setRemoveAllDownload() {

        // 다운로드 서비스 요청 상태에서는 이벤트 실행하지 않음
        if (mDownloadServiceRequest) {
            Logger.d("download-service request status");
            return;
        }

        // 순차 파일 삭제
        for (ZoneDownloadReqData data : mListDownloadReq) {

            // 다운로드 완료된 파일이 아닌 경우에만 파일 삭제
            if (data.downloadResult != RESULT_SUCCESS) {
                ContentFileManager.getInstance(IMGApplication.context).deleteFile(data.filePath);
            }
        }

        // 다운로드 요청 테이블 삭제
        mDB.deleteAllDownloadReq();

        // ArrayList 전체 삭제
        mListDownloadReq.clear();
        mListListener.clear();

        if (mLayoutProgress != null) {
            mLayoutProgress.removeAllViews();
        }
    }

    /**
     * 전체 삭제 확인 알림
     */
    private void dialogRemoveAllDownload() {

        Lib.toaster(IMGApplication.context, "remove all download");

        /*if (getActivity() != null) {

            // 활성 다운로드 있음
            //if (mActiveDownload != null) {
            try {
                if(mService.getActiveDownload() != null) {
                    //handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);
                    //dialogExistActiveDownload();
                    dialogConfirmDeleteAllDownload();
                    return;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            if (existDownloadReq()) {
                DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ALL, this);
                dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
            } else {
                dialogNoDownload();
            }
        }*/
    }

    /**
     * 다운로드 항목 없음 표출
     */
    private void dialogNoDownload() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NO_DOWNLOAD, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 외장 sd 카드 없음 alert 표출
     */
    private void dialogNoExtSDCard() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NO_EXT_SDCARD, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 다른 활성 다운로드 존재 알림
     */
    private void dialogExistActiveDownload() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_EXIST_ACTIVE_DOWNLOAD, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 네트워크 없음 알림
     */
    private void dialogNoNetwork(int index) {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NO_NETWORK, index, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 미디어 마운트 에러(저장소 제거) 알림
     */
    private void dialogMediaMountError() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_MEDIA_MOUNT_ERROR, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 저장소 위치 다름
     */
    private void dialogNotMatchingLocation(String msg) {

        if (getActivity() != null) {

            String message = getString(R.string.message_check_storage, msg);

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NOT_MATCHING_LOCATION, 0, message, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 활성 다운로드 삭제 alert 표출
     * @param position 삭제할 item index
     */
    private void dialogConfirmDeleteDownload(int position) {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_CONFIRM_DELETE_ACTIVE_DOWNLOAD, position, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 활성 다운로드를 포함한 전체 삭제 alert 표출
     */
    private void dialogConfirmDeleteAllDownload() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_CONFIRM_DELETE_ALL, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 일반 다운로드 삭제 alert 표출
     * @param position 삭제할 item index
     */
    private void dialogConfirmDelete(int position) {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_CONFIRM_DELETE_ITEM, position, this);
        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * 유효 공간 부족 alert
     */
    private void dialogUnusableSpace() {

        handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);

        if (getActivity() != null) {

            // 저장소 제거로 인한 표출 차단
            if (mMediaMountError) {
                return;
            }

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_UNUSABLE_SPACE, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 네트워크 데이터 과금 Alert 표출
     */
    private void dialogNetworkAlert() {

        if (getActivity() != null) {

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NETWORK_WARNING, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 중복 다운로드 있음 alert 표출
     * @param total     전체 개수
     * @param duplicate 중복 개수
     */
    @SuppressLint("DefaultLocale")
    private void dialogDownloadCount(int total, int duplicate) {

        // 총 41개 중 이미 다운로드 된 N개를 제외한 40개의 콘텐츠가 다운로드 목록에 추가 되었습니다.
        // 이미 다운로드 된 콘텐츠 입니다. \n 보관함을 확인하세요.
        // 총 41개 중 41개 콘텐츠가 다운로드 목록에 추가 되었습니다.
        if (getActivity() != null) {
            String message;

            if (duplicate == 0) {

                // 총 %d 개 중 %d 개 다운로드 목록 추가
                message = getString(R.string.message_add_download_0, Integer.toString(total), Integer.toString(total));
                        /*String.format("총 %d 개 중 %d 개 다운로드 목록 추가",
                        total,
                        total);*/
            } else {

                if (total == duplicate) {

                    message = IMGApplication.context.getString(R.string.dialog_already_exist);

                } else {

                    // 총 %d 개 중 %d 개 다운로드 목록 추가. %d 개는 다운로드 목록이나 보관함을 확인하세요.
                    message = getString(R.string.message_add_download_1, Integer.toString(total - duplicate),
                            Integer.toString(total), Integer.toString(duplicate));
                            /*String.format("총 %d 개 중 %d 개 다운로드 목록 추가. %d 개는 다운로드 목록이나 보관함을 확인하세요.",
                            total, (total - duplicate), duplicate);*/
                }
            }

            DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_COUNT, -1, message, this);
            dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    private class TestTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            for (int i = 0; i < 8; i++) {

                ZoneDownloadReqData data = new ZoneDownloadReqData();
                DownloadItemListener listener = new DownloadItemListener();

                data.downloadID = i;
                data.siteID = "59";
                data.siteName = "대성마이맥";
                data.userID = "guest";
                data.videoQuality = "HD";
                data.durationTime = "3621";
                data.fileName = "/media/MIMAC/PRIVATE/LEC/2016/L161105020/L161105020_46_H.mp4";
                data.isCert = 1;
                data.courseID = "abcd";
                data.courseName = "패키지 A";
                data.lectureID = "000" + i;
                data.lectureName = "안철우 선생님 " + i;

                // RecycledView 에 추가
                addDownloadItem(data, listener, false);
            }

        }
    }

    /**
     * Intent 데이터 및 저장 데이터 처리 AsyncTask
     * @author kimsanghwan
     * @since 2015.11.02.
     */
    private class DataProcessingTask extends AsyncTask<Void, Void, ArrayList<ZoneDownloadReqData>> {

        // 신규 추가 다운로드 진행 인덱스
        private int startIndex = -1;

        private int duplicatedCount = 0;    // 중복 요청 다운로드 개수
        private int totalCount = 0;         // 전체 요청 다운로드 개수

        // 신규 추가 다운로드 요청 리스트
        private ArrayList<ZoneDownloadReqData> listNewItem = new ArrayList<>();

        @Override
        protected ArrayList<ZoneDownloadReqData> doInBackground(Void... params) {

            ArrayList<ZoneDownloadReqData> reqDataArrayList = initData();

            if (reqDataArrayList != null) {

                totalCount = reqDataArrayList.size();

                for (ZoneDownloadReqData data : reqDataArrayList) {

                    // 중복 다운로드 요청 처리
                    if (mDB.existDownloadReqInfo(data) || mDB.existFileByDownloadReq(data)) {

                        // 중복 다운로드 개수 누적
                        duplicatedCount++;

                        // 신규 다운로드 요청 처리
                    } else {

                        // 최종 다운로드 인덱스 획득
                        int lastIndex = mDB.getLastIndexDownloadReq();

                        // 신규 다운로드 아이템 중 1st 아이템의 인덱스 설정
                        if (startIndex < 0) {

                            // onPostExecute() 에서 mListDownloadReq.add() 예정이므로 현재 getDownloadReqInfoCount() 설정
                            startIndex = mDB.getDownloadReqInfoCount();
                        }

                        // 다운로드 요청 데이터를 DB에 추가
                        mDB.addDownloadReqInfo(data, (++lastIndex), lastIndex, System.currentTimeMillis());

                        // 신규 다운로드 요청 데이터 추가
                        listNewItem.add(data);
                    }
                }
            }

            return mDB.getDownloadReqInfo();
        }

        @Override
        protected void onPostExecute(ArrayList<ZoneDownloadReqData> list) {

            super.onPostExecute(list);
            // 다운로드 요청 결과가 없으면 처리 완료
            if (list == null) {
                return;
            }

            // 신규 다운로드 데이터를 RecycledView 에 추가
            for (ZoneDownloadReqData data : listNewItem) {

                // 다운로드 아이템 리스너 생성
                DownloadItemListener listener = new DownloadItemListener();

                // RecycledView 에 추가
                addDownloadItem(data, listener, true);
            }

            // 신규 다운로드 요청이 있음
            if (totalCount > 0) {

                // 신규 다운로드 내용 알림 표시
                dialogDownloadCount(totalCount, duplicatedCount);

                // 기존 활성 다운로드 획득
                mActiveDownload = DownloadService4.getActiveDownload();

                // 기존 활성 다운로드 있음
                if (mActiveDownload != null) {

                    // ActiveDownload 를 해당 위치에 교체 삽입
                    for (int i = 0; i < mListDownloadReq.size(); i++) {

                        int downloadId = mListDownloadReq.get(i).downloadID;
                        if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == downloadId) {

                            ((DownloadItemListener) (mActiveDownload.DOWNLOAD_LISTENER))
                                    .setView(mListListener.get(i).view);
                            mUpdateProgress = false;

                            break;
                        }
                    }

                    // 기존 활성 다운로드 없음
                } else {

                    // 인텐트 데이터 파싱 정상 완료 후 신규 다운로드 아이템 존재
                    if (startIndex > -1) {

                        // Message 전달
                        handler.removeMessages(MSG_RESUME_DOWNLOAD);

                        Message message = handler.obtainMessage(MSG_RESUME_DOWNLOAD, startIndex, -1);
                        handler.sendMessageDelayed(message, 300);	// Message 전달
                    }
                }
            }

            // 전달된 Intent 의 파싱 처리가 완료되었으므로, 기존 Intent 초기화
            mIntent = null;
        }
    }

    /**
     * 다운로드 아이템 이벤트 리스너
     */
    private class DownloadItemListener implements IDownloadItemListener {

        private int position;
        private View view;

        /**
         * 생성자
         * @param index Index
         * @param view  View
         */
        public void setView(int index, View view) {

            this.position = index;
            this.view = view;
        }

        /**
         * 생성자
         * @param view  View
         */
        public void setView(View view) {

            this.view = view;
        }

        @Override
        public void onUpdateProgress(int downloadID, long total, long progress)
                throws RemoteException {

            if (view == null) {
                Logger.d("view is null");
                return;
            }

            int index = getIndexByDownloadID(downloadID);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.fileSize = total;
                reqData.downloadSize = progress;

                if (! mUpdateProgress) {
                    mUpdateProgress = true;

                    // Message 생성
                    Message message = handler.obtainMessage(MSG_UPDATE_PROGRESS, index, -1, view);	// Progress Index 설정
                    handler.sendMessageDelayed(message, TIME_UPDATE_PROGRESS);	// Message 전달
                }
            }
        }

        @Override
        public void onNotifyStatus(int downloadID, int status, int result, String msg, int certCode, String certMsg) throws RemoteException {

            if (view == null) {
                return;
            }

            // 다운로드 id로 현재 list index 획득
            int index = getIndexByDownloadID(downloadID);

            //Logger.d("[" + index + "] status: " + status + ", result: " + result + ", msg: " + msg);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.downloadStatus = status;
                reqData.downloadResult = result;
                reqData.downloadMsg = msg;

                // 2. Message 생성 및 전달
                Message message = handler.obtainMessage(MSG_NOTIFY_STATUS, index, status, view);

                Bundle data = new Bundle();
                data.putInt(KEY_MSG_STATUS, status);
                data.putString(KEY_MSG_MESSAGE, msg);
                data.putInt(KEY_CERT_CODE, certCode);
                data.putString(KEY_CERT_MESSAGE, certMsg);

                message.setData(data);

                handler.sendMessage(message);
            }
        }

        @Override
        public void onGetFileInformation(int downloadID, int status, int result,
                                         long totalSize, long createDate, long startDate, long endDate)
                throws RemoteException {

            if (view == null) {
                return;
            }

            // 다운로드 id로 현재 list index 획득
            int index = getIndexByDownloadID(downloadID);

            //Logger.d("[" + index + "] status: " + status + ", result: " + result + ", totalSize: " + totalSize + ", createData: " + createDate);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.downloadStatus = status;
                reqData.downloadResult = result;

                if (totalSize > 0) {
                    reqData.fileSize = totalSize;
                }
                if (createDate > 0) {
                    reqData.createTime = createDate;
                }
                /*if (startDate > 0) {
                    reqData.certStartTime = startDate;
                }
                if (endDate > 0) {
                    reqData.certEndTime = endDate;
                }*/

                // 2. 메시지 생성 및 전달
                Message message = handler.obtainMessage(MSG_FILE_INFORMATION, index, status, view);
                handler.sendMessage(message);
            }
        }

        @Override
        public IBinder asBinder() {
            return null;
        }
    }

    /**
     * 다운로드 정지/재개 리스너
     * @author kimsanghwan
     * @since 2015-11-17
     */
    private class DownloadStopResumeListener implements View.OnClickListener {

        private int downloadID;     // 다운로드 id
        private ImageView viewStopResume;// 다운로드 정지 재개 버튼

        /**
         * 생성자
         * @param id    Download ID
         * @param view  View
         */
        DownloadStopResumeListener(int id,View view) {

            downloadID = id;
            viewStopResume = (ImageView) view;
        }

        @Override
        public void onClick(View v) {

            // 서비스 가드가 true 이면 return
            // 동시에 서비스 가드 재설정
            if (mServiceGuard) {
                handler.removeMessages(MSG_RELEASE_SERVICE_GUARD);
                handler.sendEmptyMessageDelayed(MSG_RELEASE_SERVICE_GUARD, TIME_SERVICE_GUARD);
                Lib.toaster(IMGApplication.context, R.string.snack_wait_a_minute);
                /*if (! Util.isSnack()) {
                    Util.snack(((MainActivity) getActivity()).getBottomNavigationBar(),
                            R.string.snack_wait_a_minute, TIME_SERVICE_GUARD);
                }*/
                return;

                // 서비스 가드가 false 이면
            } else {
                handler.sendEmptyMessageDelayed(MSG_RELEASE_SERVICE_GUARD, TIME_SERVICE_GUARD);
            }

            mServiceGuard = true;

            // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
            int index = getIndexByDownloadID(downloadID);

            // 활성 다운로드 있음
            if (mActiveDownload != null) {

                if (mService != null) {

                    // 현재 활성 다운로드
                    if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == downloadID) {

                        // 다운로드 서비스 요청 상태에서는 이벤트 실행하지 않음
                        if (mDownloadServiceRequest) {
                            Logger.d("download-service request status");
                            return;
                        }

                        handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);

                        viewStopResume.setImageResource(R.drawable.ic_file_download);

                        // 활성 다운로드 아님
                    } else {

                        dialogExistActiveDownload();
                    }
                }

                // 활성 다운로드 없음
            } else {

                // 네트워크 미연결
                if (! Lib.isNetworkAvailable(IMGApplication.context)) {

                    // 네트워크 없음 알림
                    dialogNoNetwork(index);
                    return;
                }

                // 설정: 외부 저장소
                if (ConfigurationManager.getUseExtSDCard(IMGApplication.context)) {

                    // 파일 위치: 내부
                    if (mListDownloadReq.get(index).downloadLocation == LOCATION_INNER) {
                        dialogNotMatchingLocation(getString(R.string.internal));
                        return;
                    }

                    // 설정: 내부 저장소
                } else {

                    // 파일 위치: 외부
                    if (mListDownloadReq.get(index).downloadLocation == LOCATION_EXT1) {
                        dialogNotMatchingLocation(getString(R.string.external));
                        return;
                    }

                }

                Message message = handler.obtainMessage(MSG_RESUME_DOWNLOAD, index, -1);
                handler.sendMessageDelayed(message, 1000);	// Message 전달
            }
        }
    }

    /**
     * 다운로드 요청 아이템 삭제 리스너
     * @author kimsanghwan
     * @since 2015-11-18
     */
    private class RemoveItemListener implements View.OnLongClickListener {

        private int downloadID;

        /**
         * 생성자
         * @param id    download-id
         */
        RemoveItemListener(int id) {

            downloadID = id;
        }

        /*@Override
        public void onClick(View v) {

            //Lib.toaster(IMGApplication.context, "download-id: " + downloadID);

            // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
            int index = getIndexByDownloadID(downloadID);
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // 활성 다운로드 있음
            if (mActiveDownload != null) {

                // 활성 다운로드 삭제 시도
                if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == downloadID) {
                    dialogConfirmDeleteDownload(index);
                } else {

                    // 파일경로가 존재하면 삭제
                    if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                        ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                    }

                    removeDownloadItem(index);
                }

            } else {

                // 파일경로가 존재하면 삭제
                if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                    ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                }

                removeDownloadItem(index);
            }

        }*/

        @Override
        public boolean onLongClick(View v) {

            /*//Lib.toaster(IMGApplication.context, "download-id: " + downloadID);

            // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
            int index = getIndexByDownloadID(downloadID);
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // 활성 다운로드 있음
            if (mActiveDownload != null) {

                // 활성 다운로드 삭제 시도
                if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadID == downloadID) {
                    dialogConfirmDeleteDownload(index);
                } else {

                    // 파일경로가 존재하면 삭제
                    if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                        ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                    }

                    removeDownloadItem(index);
                }

            } else {

                // 파일경로가 존재하면 삭제
                if (StringUtil.isNotBlank(reqData.filePath) && reqData.downloadResult != RESULT_SUCCESS) {
                    ContentFileManager.getInstance(IMGApplication.context).deleteFile(reqData.filePath);
                }

                removeDownloadItem(index);
            }*/

            dialogConfirmDelete(downloadID);

            return true;
        }
    }

    /**
     * 네트워크 상태 변화를 수신하는 BroadcastReceiver
     * @author kimsanghwan
     * @since 2014. 3. 27.
     */
    private class NetworkStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if ((intent != null) && (IMGApplication.context != null)) {

                String action = intent.getAction();

                if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    ConnectivityManager conMgr = (ConnectivityManager) IMGApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                    if (netInfo == null) {
                        //Lib.toaster(IMGApplication.context, "네트워크 오류");
                        Util.snack(getView(), R.string.network_error, 2000);
                        return;
                    }

                    if (! netInfo.isAvailable()) {
                        Util.snack(getView(), R.string.network_error, 2000);
                    }
                }
            }

        }

    }

    /**
     * 미디어 마운트 리시버
     * @author kimsanghwan
     * @since 2015-11-20
     */
    private class MediaMountReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String strAction = intent.getAction();

            if (IMGApplication.context != null) {

                // SD Card Mounted
                if (strAction.compareTo(Intent.ACTION_MEDIA_SCANNER_FINISHED) == 0) {
                    Lib.toaster(IMGApplication.context, "media-scan finished");
                    // Logger.d("media scan finished");
                } else if (strAction.compareTo(Intent.ACTION_MEDIA_EJECT) == 0) {
                    // Util.toaster(IMGApplication.context, "media unmounted");

                    mMediaMountError = true;
                    dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
                    ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);

                } else if (strAction.compareTo(Intent.ACTION_MEDIA_MOUNTED) == 0) {
                    Lib.toaster(IMGApplication.context, "media mounted");
                    mMediaMountError = false;
                } else if (strAction.compareTo(Intent.ACTION_MEDIA_UNMOUNTED) == 0) {
                    mMediaMountError = true;
                    //dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
                    ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);
                    Lib.toaster(IMGApplication.context, "media unmounted");

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);

                }  else if (strAction.compareTo(Intent.ACTION_MEDIA_REMOVED) == 0) {
                    mMediaMountError = true;
                    //dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.context, false);
                    ContentFileManager.getInstance(IMGApplication.context).setUseExtSDCard(false);
                    Lib.toaster(IMGApplication.context, "media removed");

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD);
                }
            }

        }
    }

    /**
     * 다운로드 Progress 관리 Handler
     * @author kimsanghwan
     * @since 2014. 8. 29.
     */
    private static class DownloadProgressHandler extends WeakHandler<DownloadManager6> {

        DownloadProgressHandler(DownloadManager6 owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {

            DownloadManager6 owner = getOwner();

            switch (msg.what) {

                // 다운로드 진행 Progress 업데이트 메시지
                case MSG_UPDATE_PROGRESS:

                    owner.progressItemView((View) msg.obj, msg.arg1);
                    DownloadManager6.mUpdateProgress = false;
                    break;

                // 다운로드 STEP 메시지
                case MSG_NOTIFY_STATUS:

                    owner.statusItemView((View) msg.obj, msg.arg1);

                    Bundle bundle = msg.getData();
                    int certCode = bundle.getInt(KEY_CERT_CODE);
                    String certMsg = bundle.getString(KEY_CERT_MESSAGE);

                    if ((certCode == RET_CUSTOMER_ERROR) && (StringUtil.isNotBlank(certMsg))) {
                        if (IMGApplication.context != null) {
                            //Lib.toaster(IMGApplication.context, certMsg, Toast.LENGTH_LONG);
                            Util.snack(owner.getView(), certMsg, 4000);
                        }
                    }
                    break;

                // 인증서 및 파일 정보 전달 메시지
                case MSG_FILE_INFORMATION:

                    owner.infoItemView((View) msg.obj, msg.arg1);
                    break;

                // 다운로드 아이템 뷰 표출
                case MSG_START_DOWNLOAD_PROGRESS:

                    // 전달받은 다운로드 Intent 시작
                    owner.startDownloadProgress();
                    break;

                // Notification 해제
                case MSG_END_NOTIFICATION:

                    if (owner != null) {
                        owner.releaseNotification();
                    }
                    break;

                // 다운로드 재개 시작
                case MSG_RESUME_DOWNLOAD:

                    // 다운로드 유효 공간 확인
                    if (!owner.isUsableSpace()) {

                        owner.dialogUnusableSpace();
                        return;
                    }

                    // 활성 다운로드 생성
                    mActiveDownload = new ActiveDownload(
                            mListDownloadReq.get(msg.arg1),
                            mListListener.get(msg.arg1));


                    // DownloadState Broadcast 전송. 다운로드 시작
                    owner.sendDownloadStateBroadcast(msg.arg1, 1);

                    owner.startServiceBind();

                    break;

                // 다운로드 서비스 정지
                case MSG_STOP_DOWNLOAD:

                    if (mService != null) {

                        try {
                            mService.stopDownload();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        // progress-update 가 delay 부여되어 있으므로
                        // update 완료 후 처리하도록 1000ms delay 부여
                        handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);

                        // 다운로드 서비스 언바인드
                        owner.unbindService();

                        owner.mBlockNotification = true;
                    }

                    break;

                // 다운로드 재개 시작
                case MSG_NEXT_DOWNLOAD:

                    // 다운로드 유효 공간 확인
                    if (!owner.isUsableSpace()) {

                        owner.dialogUnusableSpace();
                        return;
                    }

                    // 활성 다운로드 생성
                    mActiveDownload = new ActiveDownload(
                            mListDownloadReq.get(msg.arg1),
                            mListListener.get(msg.arg1));

                    // DownloadState Broadcast 전송. 다운로드 시작
                    owner.sendDownloadStateBroadcast(msg.arg1, 1);

                    DownloadManager6.mUpdateProgress = false;

                    // DownloadService4 가 있으면 바로 다운로드 이어서 진행
                    if (mService != null) {

                        // 서비스 시작
                        try {
                            mService.setActiveDownload(mActiveDownload);
                            mService.resumeDownload();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        // mNotification 시작
                        owner.startNotification(mActiveDownload.DOWNLOAD_REQ_DATA.lectureName);

                        // DownloadService4 가 없으면 먼저 생성 및 바인드
                        // 이후 신규 다운로드와 같이 시작
                    } else {

                        owner.startServiceBind();
                    }

                    break;

                case MSG_ADD_ROW_VIEW:
                    if (DownloadManager6.mLayoutProgress != null) {
                        DownloadManager6.mLayoutProgress.addView((View)(msg.obj));
                    }
                    break;

                case MSG_RELEASE_SERVICE_GUARD:
                    owner.mServiceGuard = false;
                    break;

            }
        }
    }
}
