package kr.hri.sample.player;

/**
 * Back Key Click Listener
 * @author kimsanghwan
 * @since  2015. 1. 28.
 */
public interface IBackPressedListener {

	/**
	 * Back Key Press
	 */
	void onBackPressed();
}
