package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Browser;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.andlib.helpers.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

import kr.hri.sample.player.component.CourseAdapter;
import kr.hri.sample.player.component.DividerItemDecoration;
import kr.hri.sample.player.component.ItemTouchHelperCallback;
import kr.hri.sample.player.widget.DownloadDialog;
import kr.hri.sample.player.widget.ProgressingDialog;
import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.CourseInfoData;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.ItemTouchHelperExtension;
import kr.imgtech.lib.zoneplayer.network.NetworkManager;
import kr.imgtech.lib.zoneplayer.util.Lib;
import kr.imgtech.lib.zoneplayer.util.StringUtil;

/**
 * 강좌 그룹 Fragment
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class CourseFragment extends Fragment implements IBackPressedListener,
        IntentDataDefine,
        BaseInterface,
        BaseDialogListener,
        CourseAdapter.CourseAdapterListener {

    @SuppressLint("StaticFieldLeak")
    private static CourseFragment instance;

    private ArrayList<CourseInfoData> mListCourse = new ArrayList<>();  // 강좌 그룹 ArrayList
    private CourseAdapter mAdapter;                                     // 강좌 Adapter
    private ItemTouchHelperExtension mItemTouchHelper;
    private ItemTouchHelperExtension.Callback mHelperCallback;

    private ContentsDatabase2 mDB;
    private ProgressingDialog mProgressingDialog;

    private Intent mIntent;
    private String mSiteID;
    private String mUserID;
    private String mRequestURL;
    private String mDeleteURL;
    private ArrayList<ArrayList<ZoneDownloadData>> mLectureList = new ArrayList<>();

    private RecyclerView mCourseRecyclerView;

    /**
     * 생성자
     */
    public CourseFragment() {

        super();

        mIntent = null;
    }

    /**
     * instance 반환
     * @return  CourseFragment
     */
    public static CourseFragment getInstance() {

        if (instance == null) {
            instance = new CourseFragment();
        } else {
            instance.mIntent = null;
        }

        return instance;
    }

    /**
     * instance 반환
     * @param bundle    Bundle
     * @return  CourseFragment
     */
    public static CourseFragment getInstance(Bundle bundle) {

        instance = getInstance();

        if (bundle != null) {

            instance.mIntent = bundle.getParcelable(MainActivity.KEY_INTENT);
        }

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mDB = ContentsDatabase2.getInstance(getContext());

        setHasOptionsMenu(true);
        return initView(inflater, container);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        new UpdateItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mAdapter.getCourseList());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mListCourse != null) {
            mListCourse.clear();
        }

        if (mAdapter != null) {
            mAdapter = null;
        }
    }

    @Override
    public void onBackPressed() {

        ((MainActivity) getActivity()).finishApplicationProcess();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.toolbar, menu);

        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.toolbar_download:

                Uri.Builder uriBuilder = new Uri.Builder()
                        .scheme(getString(R.string.scheme_hri))
                        .authority(getString(R.string.host_download))
                        .appendQueryParameter(SITE_ID, MainActivity.HRI_ID)

                        // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                        .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                        .appendQueryParameter(DATA, "download;guest;안철우 선생님")
                        ;

                Logger.d(uriBuilder.toString());

                // 테스트 URL 로 실행 Intent 생성
                Intent downloadIntent = null;
                try {
                    downloadIntent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                // Bundle 에 생성한 Intent 설정
                Bundle bundle = new Bundle();
                bundle.putParcelable(MainActivity.KEY_INTENT, downloadIntent);

                // 다운로드 샐행
                DownloadManager6.getInstance(bundle).startDownloadManager();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialog(int state, int code, int result) {

        switch (state) {

            case DIALOG_DOWNLOAD_DELETE_ITEM:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {
                    //Lib.toaster(getContext(), "code: " + code);
                    new DeleteItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, code);
                }

                break;
        }
    }

    @Override
    public void onItemSelectedListener(int position, boolean isCert, int remainTime) {

        if (StringUtil.isNotBlank(mRequestURL) && (isCert) && (remainTime < 1)) {

            new RequestAsyncTask().execute(position);
        } else {

            // Adapter 에서 강좌 정보 획득
            CourseInfoData data = mAdapter.getItem(position);

            if (data != null) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(MainActivity.KEY_INTENT,
                        makeContentIntent(mSiteID, mUserID,
                                data.courseID, data.teacherName, data.courseName, data.isCert, data.endTime, data.remainTime, data.courseImagePath));

                ((MainActivity) getActivity()).switchFragmentDelay(MainActivity.ID_BOX_LECTURE, bundle);
            }
        }

    }

    @Override
    public void onDeleteSelectedListener(int position) {

        dialogDeleteItem(position);
    }

    private View initView(LayoutInflater in, ViewGroup viewGroup) {

        View view = in.inflate(R.layout.fragment_box, viewGroup, false);

        mCourseRecyclerView = view.findViewById(R.id.rv_course);
        mCourseRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        mCourseRecyclerView.setHasFixedSize(true);

        mAdapter = new CourseAdapter(getContext());
        mAdapter.setOnSelectedListener(this);
        mCourseRecyclerView.setAdapter(mAdapter);
        mCourseRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mCourseRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mHelperCallback = new ItemTouchHelperCallback();
        mItemTouchHelper = new ItemTouchHelperExtension(mHelperCallback);
        mItemTouchHelper.attachToRecyclerView(mCourseRecyclerView);
        mAdapter.setItemTouchHelperExtension(mItemTouchHelper);
        mAdapter.setCourseList(mListCourse);

        if ((mIntent != null) && (mListCourse != null)) {

            Uri uri = mIntent.getData();
            if (uri != null) {

                mSiteID = uri.getQueryParameter(SITE_ID);
                mUserID = uri.getQueryParameter(USER_ID);
                mRequestURL = uri.getQueryParameter(MainActivity.KEY_REQUEST_URL);
                mDeleteURL = uri.getQueryParameter(MainActivity.KEY_DELETE_URL);

                // 강좌 목록 반환
                ArrayList<CourseInfoData> courseList = mDB.getCourseInfoDataBySiteID_UserID(mSiteID, mUserID);

                int index = 0;
                for (CourseInfoData course : courseList) {

                    // 강의 목록 반환
                    mLectureList.add(mDB.getFileInfoByCourseID(mSiteID, mUserID, course.courseID));

                    if ((mLectureList != null) && (mLectureList.size() > 0)) {

                        ZoneDownloadData lecture = mLectureList.get(index++).get(0);

                        course.isCert = (lecture.isCert > 0);
                        course.endTime = getFormatDate(lecture.certEndTime);
                        course.intRemainTime = getIntRemainTime(lecture.certEndTime);
                        course.remainTime = getRemainTime(lecture.certEndTime);
                    } else {

                        course.endTime = "";
                        course.remainTime = "";
                    }

                    mListCourse.add(course);
                }

                mAdapter.updateList(mListCourse);

                /*ArrayList<ZoneDownloadData> listData = mDB.getAllFileInfoDetail(mSiteID);

                if ((listData != null) && (listData.size() > 0)) {

                    mListCourse = mDB.getCourseInfoDataBySiteId(mSiteID);
                    if ((mListCourse != null) && (mListCourse.size() > 0)) {
                        // todo 해당 Site ID 강좌가 있는 경우
                    } else {
                        // todo 해당 Site ID 강좌가 없는 경우
                    }
                }*/
            }
        }

        //testList();

        return view;
    }

    /**
     * Time 을 날짜 형식 문자로 반환
     * @param date	Time (초 단위)
     * @return 날짜 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDate(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return String.format("%04d.%02d.%02d", year, mon + 1, day);
    }

    /**
     * 남은 기간 반환
     * @param endTime   강의 종료 시간
     * @return  남은 기간
     */
    private int getIntRemainTime(long endTime) {

        return (int) (((endTime) - (System.currentTimeMillis() / 1000)) / (60 * 60 * 24) + 1);
    }

    /**
     * 남은 기간 반환
     * @param endTime   강의 종료 시간
     * @return  남은 기간
     */
    private String getRemainTime(long endTime) {

        return Long.toString(((endTime) - (System.currentTimeMillis() / 1000)) / (60 * 60 * 24) + 1);
    }

    private void testList() {

        if (mListCourse == null) {
            return;
        }

        for (int i = 0; i < 4; i++) {

            CourseInfoData data = new CourseInfoData();
            data.courseID = Integer.toString(i);

            switch (i) {
                case 0:
                    data.courseImageResID = R.drawable.c_362_0017;
                    data.courseName = "[2018] 원하는 것 보다 더! 1+ 期[기]특한 사회문화 문제풀이";
                    data.teacherName = "사회/배인영";
                    break;
                case 1:
                    data.courseImageResID = R.drawable.c_455_0017;
                    data.courseName = "2018 물리Ⅰ 실전 모의고사 [진검승부] Vol.1";
                    data.teacherName = "물리/안철우";
                    break;
                case 2:
                    data.courseImageResID = R.drawable.c_531_0017;
                    data.courseName = "[2018] 파이널 패키지 [그럼에도 불구하고 + 실전모의고사 1]";
                    data.teacherName = "영어/이명학";
                    break;
                case 3:
                    data.courseImageResID = R.drawable.c_935_0017;
                    data.courseName = "[2018] 화학Ⅰ同形 모의고사 Season 1";
                    data.teacherName = "화학/우마리아";
                    break;
            }

            mListCourse.add(data);
        }

        mAdapter.updateList(mListCourse);

        mAdapter.notifyDataSetChanged();

    }

    /**
     * Lecture Fragment Intent 생성
     * @param siteID	Site ID
     * @param courseID  Course ID
     * @return	Course Group Fragment Intent
     */
    private Intent makeContentIntent(String siteID, String userID, String courseID,
                                     String teacher, String courseName, boolean isCert, String endTime, String remainTime, String courseImage) {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority("lecture-list")
                .appendQueryParameter(SITE_ID, siteID)
                .appendQueryParameter(USER_ID, userID)
                .appendQueryParameter(COURSE_ID, courseID)
                .appendQueryParameter(TEACHER_NAME, teacher)
                .appendQueryParameter(COURSE_NAME, courseName)
                .appendQueryParameter(IS_CERT, (isCert ? "1" : "0"))
                .appendQueryParameter(END_TIME, endTime)
                .appendQueryParameter("remain-time", remainTime)
                .appendQueryParameter(COURSE_IMAGE, courseImage)
                ;

        Logger.d(uriBuilder.toString());

        Intent intent = null;
        // Intent 설정
        try {
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
            intent.setPackage(getActivity().getPackageName());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return intent;
    }

    /**
     * 보관함 강의 삭제 콜백
     * @param data      삭제한 강의 ZoneDownloadData
     */
    private void sendDeleteInfo(ZoneDownloadData data) {

        if (StringUtil.isBlank(mDeleteURL)) {
            return;
        }

        String encodedExtInfo = "";
        try {
            encodedExtInfo = URLEncoder.encode(data.extInfo, "EUC-KR");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringBuilder parameterString = new StringBuilder("?");
        parameterString.append(USER_ID + "=" + mUserID + "&");
        parameterString.append(EXT_INFO + "=" + encodedExtInfo);

        // URL + URL 인코딩 문자열 전송
        new NetworkManager(getContext()).httpGet(mDeleteURL + parameterString);
    }

    /**
     * 해당 강좌 내 모든 파일 및 파일 정보 삭제
     * @param course 강좌 정보
     */
    private void deleteAllFileInfoInCourse(CourseInfoData course) {

        // 강좌에 해당하는 전체 파일 정보 획득
        ArrayList<ZoneDownloadData> listData
                = ContentsDatabase2.getInstance(
                getActivity()).getFileInfoByCourseID(course.siteID, course.userID, course.courseID);

        // 강좌에 해당하는 전체 파일과 파일 정보 삭제
        for (ZoneDownloadData data : listData) {

            // 파일 삭제
            ContentFileManager.getInstance(getActivity()).deleteFile(data.filePath);

            // DB 삭제 처리
            ContentsDatabase2.getInstance(getActivity()).deleteFileInfoDetail(data);

            // 삭제 콜백
            sendDeleteInfo(data);

            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 강의 정보 재요청 및 응답 수신
     * @param data  재요청할 강의 정보
     * @return  재요청 응답
     */
    private String requestLectureInfo(ZoneDownloadData data) {

        String encodedExtInfo = "";
        try {
            encodedExtInfo = URLEncoder.encode(data.extInfo, "EUC-KR");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringBuilder parameterString = new StringBuilder("?");
        parameterString.append(USER_ID + "=" + data.userID + "&");
        parameterString.append(EXT_INFO + "=" + encodedExtInfo);

        // URL + URL 인코딩 문자열 전송
        return new NetworkManager(getContext()).httpGet(mRequestURL + parameterString);
    }

    /**
     * 응답 데이터를 ZoneDownloadData 로 파싱
     * @param responseString    응답 문자열
     * @return  ZoneDownloadData
     */
    private ZoneDownloadData parseRequestLectureInfo(String responseString) {

        if (StringUtil.isBlank(responseString)) {
            return null;
        }

        ZoneDownloadData data = null;

        // JSONObject 생성
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        String code = null, message;
        String siteID = null, userID = null, siteName = null;
        // 파싱 시작
        try {

            if (jsonObj.has(CODE)) {
                code = jsonObj.getString(CODE);
            }

            // 오류 응답 코드
            if (StringUtil.equals(CODE_FALSE, code)) {

                if (jsonObj.has(MESSAGE)) {
                    message = jsonObj.getString(MESSAGE);
                } else {
                    message = "서비스를 실행할 수 없습니다. 고객센터로 연락바랍니다.";
                }

                return null;
            }

            // site-id
            if (jsonObj.has(SITE_ID)) {
                siteID = jsonObj.getString(SITE_ID);
            }

            // user-id
            if (jsonObj.has(USER_ID)) {
                userID = jsonObj.getString(USER_ID);
            }

            // site-name
            if (jsonObj.has(SITE_NAME)) {
                siteName = jsonObj.getString(SITE_NAME);
            }

            // Contents
            JSONArray jsonArray = null;
            if (jsonObj.has(CONTENTS)) {
                jsonArray = jsonObj.getJSONArray(CONTENTS);
            }
            if (jsonArray != null) {

                // 최초 0번째 JSONObject 만 파싱
                for (int i = 0; i < 1; i++) {

                    data = new ZoneDownloadData();

                    // 일괄 적용
                    data.siteID = siteID;
                    data.userID = userID;
                    data.siteName = siteName;

                    // 상세 정보
                    JSONObject json = jsonArray.getJSONObject(i);

                    // file-name
                    if (json.has(FILE_NAME)) {
                        data.fileName = json.getString(FILE_NAME);
                    } else {
                        return null;
                    }

                    // is-cert
                    if (json.has(IS_CERT)) {
                        try {
                            data.isCert = Byte.parseByte(json.getString(IS_CERT));
                        } catch (NumberFormatException | JSONException e) {
                            e.printStackTrace();
                            data.isCert = 0;
                        }
                        // cert-start-time
                        try {
                            if (json.has(CERT_START_TIME)) {
                                data.certStartTime = Long.parseLong(json.getString(CERT_START_TIME));
                            }
                        } catch (NumberFormatException | JSONException e) {
                            e.printStackTrace();
                        }
                        // cert-end-time
                        try {
                            if (json.has(CERT_END_TIME)) {
                                data.certEndTime = Long.parseLong(json.getString(CERT_END_TIME));
                            }
                        } catch (NumberFormatException | JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        data.isCert = 0;
                    }

                    // course-id
                    if (json.has(COURSE_ID)) {
                        data.courseID = json.getString(COURSE_ID);
                    }
                    // course-name
                    if (json.has(COURSE_NAME)) {
                        data.courseName = json.getString(COURSE_NAME);
                    }

                    // lecture-id
                    if (json.has(LECTURE_ID)) {
                        data.lectureID = json.getString(LECTURE_ID);
                    }
                    // lecture-name
                    if (json.has(LECTURE_NAME)) {
                        data.lectureName = json.getString(LECTURE_NAME);
                    }

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return data;
    }

    /**
     * 삭제 Alert 표출
     * @param position  삭제 아이템 position
     */
    private void dialogDeleteItem(int position) {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ITEM, position, this);
        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * ProgressDialog 표출
     */
    private void dialogWaiting() {

        if (getActivity() == null) {
            return;
        }

        mProgressingDialog = ProgressingDialog.getInstance(0);
        mProgressingDialog.show(getActivity().getSupportFragmentManager(), ProgressingDialog.DIALOG_TAG);
    }

    /**
     * ProgressDialog 해제
     */
    private void dismissWaiting() {

        if (getActivity() == null) {
            return;
        }

        if (mProgressingDialog != null) {
            mProgressingDialog.dismiss();
            mProgressingDialog = null;
        }
    }

    /**
     * 강좌 그룹 삭제 AsyncTask
     */
    private class DeleteItemAsyncTask extends AsyncTask<Integer, Void, ArrayList<CourseInfoData>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogWaiting();
        }

        @Override
        protected ArrayList<CourseInfoData> doInBackground(Integer... params) {

            ArrayList<CourseInfoData> listDelete = new ArrayList<>();

            CourseInfoData courseInfo = mAdapter.getItem(params[0]);

            if (courseInfo != null) {

                // 포함된 강의 영상 파일 및 DB 삭제
                deleteAllFileInfoInCourse(courseInfo);

                // DB 에서 강좌 그룹 삭제
                ContentsDatabase2.getInstance(getActivity()).deleteCourseInfo(courseInfo.siteID, courseInfo.userID, courseInfo.courseID);

                listDelete.add(courseInfo);

                mLectureList.remove(params[0].intValue());
            }

            return listDelete;
        }

        @Override
        protected void onPostExecute(ArrayList<CourseInfoData> result) {
            super.onPostExecute(result);

            if (result != null) {

                // Adapter 삭제
                for (CourseInfoData data : result) {
                    mAdapter.removeItem(data);
                }
            }

            dismissWaiting();
        }
    }

    /**
     * 강좌 그룹 정보 업데이트 AsyncTask
     */
    private class UpdateItemAsyncTask extends AsyncTask<ArrayList<CourseInfoData>, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(ArrayList<CourseInfoData>... params) {

            if (params != null && params[0] != null) {

                int index = 0;
                ArrayList<CourseInfoData> it = (ArrayList<CourseInfoData>) params[0].clone();

                for (CourseInfoData info:it) {

                    info.courseSeq = index++;

                    mDB.updateCourseInfo(info);
                }
            }

            return null;
        }
    }

    private class RequestAsyncTask extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // ProgressDialog 표시
            dialogWaiting();
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            // ZoneDownloadData List 에서 현재 선택한 강좌 그룹의 0번째 강의 획득
            ZoneDownloadData requestData = mLectureList.get(params[0]).get(0);

            // 서버에 ZoneDownloadData 재요청
            String response = requestLectureInfo(requestData);

            // 서버로부터 받은 응답 파싱
            ZoneDownloadData responseData = parseRequestLectureInfo(response);

            // 기존 데이터와 재요청으로 받은 데이터의 인증 기간이 다르면
            if ((responseData != null) && (requestData.certEndTime != responseData.certEndTime)) {

                // 0번째 강의 인증 기간이 다른 강의 목록 획득
                ArrayList<ZoneDownloadData> needModifyList = mLectureList.get(params[0]);

                // 로컬 DB 에서 인증 기간 업데이트
                for (ZoneDownloadData data : needModifyList) {

                    data.certEndTime = responseData.certEndTime;
                    mDB.updateFileCertEndTime(data);
                }

                // ZoneDownloadData List 에서 기존 List 제거 / 재삽입
                mLectureList.remove(params[0].intValue());
                mLectureList.add(params[0], needModifyList);

                // 강좌 List 에서 현재 선택한 강좌 정보 획득
                CourseInfoData course = mListCourse.get(params[0]);

                // 신규 정보로 강좌 정보 갱신
                course.isCert = (responseData.isCert > 0);
                course.endTime = getFormatDate(responseData.certEndTime);
                course.intRemainTime = getIntRemainTime(responseData.certEndTime);
                course.remainTime = getRemainTime(responseData.certEndTime);
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(Integer index) {
            super.onPostExecute(index);

            // ProgressDialog dismiss
            dismissWaiting();

            // Adapter 갱신
            mAdapter.updateList(mListCourse);
            mAdapter.notifyDataSetChanged();

            // Adapter 에서 강좌 정보 획득
            CourseInfoData data = mAdapter.getItem(index);

            if (data == null) {
                return;
            }

            if ((data.isCert)) {

                if (data.intRemainTime > 0) {

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(MainActivity.KEY_INTENT,
                            makeContentIntent(mSiteID, mUserID,
                                    data.courseID, data.teacherName, data.courseName, data.isCert, data.endTime, data.remainTime, data.courseImagePath));

                    ((MainActivity) getActivity()).switchFragmentDelay(MainActivity.ID_BOX_LECTURE, bundle);

                } else {

                    Lib.toaster(IMGApplication.getContext(), R.string.expired);
                }

            } else {

                Bundle bundle = new Bundle();
                bundle.putParcelable(MainActivity.KEY_INTENT,
                        makeContentIntent(mSiteID, mUserID,
                                data.courseID, data.teacherName, data.courseName, data.isCert, data.endTime, data.remainTime, data.courseImagePath));

                ((MainActivity) getActivity()).switchFragmentDelay(MainActivity.ID_BOX_LECTURE, bundle);
            }

        }
    }
}
