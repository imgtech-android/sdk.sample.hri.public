package kr.hri.sample.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 다운로드 전체 상태 Receiver
 * Created by kimsanghwan on 2017. 9. 28..
 */
public class DownloadStateReceiver extends BroadcastReceiver {

    public final static String INTENT_DOWNLOAD_STATE = "kr.imgtech.lib.DownloadStateReceiver";

    // Intent Data Key
    public final static String KEY_DATA = "data";

    @Override
    public void onReceive(Context context, Intent intent) {

        String data = intent.getStringExtra(KEY_DATA);
        //Lib.toaster(context, data);
    }
}
