package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.andlib.helpers.Logger;

import java.net.URISyntaxException;

import kr.hri.sample.player.widget.DownloadDialog;
import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadService4;
import kr.imgtech.lib.zoneplayer.interfaces.EditTextDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.SingleChoiceDialogListener;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.Lib;
import kr.imgtech.lib.zoneplayer.widget.EditTextDialog;
import kr.imgtech.lib.zoneplayer.widget.SingleChoiceDialog;

/**
 * 환경설정 Fragment
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class SettingsFragment extends Fragment implements IBackPressedListener,
        IntentDataDefine,
        BaseInterface,
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        SingleChoiceDialogListener,
        EditTextDialogListener {

    private static SettingsFragment instance;

    private View mLayoutDecoder;
    private View mLayoutSkipTime;
    private View mLayoutRotation;
    private TextView mTVDecoder;
    private TextView mTVSkipTime;
    private TextView mTVRotation;
    private TextView mTVUsableSize;
    private Switch mSWExtSD;

    /**
     * 생성자
     */
    public SettingsFragment() {

        super();
    }

    /**
     * instance 반환
     * @return  CourseFragment
     */
    public static SettingsFragment getInstance() {

        if (instance == null) {
            instance = new SettingsFragment();
        }

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        return initView(inflater, container);
    }

    @Override
    public void onBackPressed() {

        ((MainActivity) getActivity()).finishApplicationProcess();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.toolbar, menu);

        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.toolbar_download:

                Uri.Builder uriBuilder = new Uri.Builder()
                        .scheme(getString(R.string.scheme_hri))
                        .authority(getString(R.string.host_download))
                        .appendQueryParameter(SITE_ID, MainActivity.HRI_ID)

                        // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                        .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                        .appendQueryParameter(DATA, "download;guest;안철우 선생님")
                        ;

                Logger.d(uriBuilder.toString());

                // 테스트 URL 로 실행 Intent 생성
                Intent downloadIntent = null;
                try {
                    downloadIntent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                // Bundle 에 생성한 Intent 설정
                Bundle bundle = new Bundle();
                bundle.putParcelable(MainActivity.KEY_INTENT, downloadIntent);

                // 다운로드 샐행
                DownloadManager6.getInstance(bundle).startDownloadManager();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v == mLayoutDecoder) {
            dialogDecoderSetting();
        } else if (v == mLayoutRotation) {
            dialogRotationSetting();
        } else if (v == mLayoutSkipTime) {
            dialogSkipSetting();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {

        if (v == mSWExtSD) {

            // 다운로드 진행 중 상태 변경 불가 알림
            if (DownloadService4.getActiveDownload() != null) {

                dialogDownloadStatus();

                mSWExtSD.setChecked(!isChecked);

                return;
            }

            // 콘텐츠 저장 위치는 실시간 변경 내용을 ContentFileManager 에도 설정 필요
            if (ContentFileManager.getInstance(getActivity()).setUseExtSDCard(isChecked)) {

                ConfigurationManager.setUseExtSDCard(getContext(), isChecked);

                // 콘텐츠 저장 위치 수정 후 재초기화
                ContentFileManager.getInstance(getActivity()).initialize();

                setStorageSize();

            } else {

                ConfigurationManager.setUseExtSDCard(getContext(), false);

                if (getActivity() != null) {
                    Lib.toaster(getActivity(), R.string.toast_no_ext_storage);
                }

                mSWExtSD.setChecked(false);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDialog(int state, String input) {

        switch (state) {
            case EditTextDialog.STATE_SEEK_TIME:

                int value;

                try {
                    value = Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    Lib.toaster(getContext(), R.string.value_actually);
                    return;
                }

                if (value < 5) {
                    Lib.toaster(getContext(), R.string.value_5sec_over);
                    return;
                }

                if (value > 60) {
                    Lib.toaster(getContext(), R.string.value_60sec_under);
                    return;
                }

                ConfigurationManager.setFastSeekTime(getContext(), value);

                if (mTVSkipTime != null) {
                    mTVSkipTime.setText(input + getResources().getString(R.string.seconds));
                }
                break;
        }
    }

    @Override
    public void onDialog(int state, int selected) {

        switch (state) {

            case SingleChoiceDialog.DECODER_OPTION:

                ConfigurationManager.setDecoderMode(getContext(), selected);
                //mTVDecoder.setText(getResources().getStringArray(R.array.decoder_option)[selected]);
                mTVDecoder.setText(IMGApplication.decoderModeName[selected]);
                break;

            case SingleChoiceDialog.ROTATION_OPTION:

                ConfigurationManager.setRotation(getContext(), selected);
                mTVRotation.setText(getResources().getStringArray(R.array.rotation_option)[selected]);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private View initView(LayoutInflater in, ViewGroup viewGroup) {

        View view = in.inflate(R.layout.fragment_settings, viewGroup, false);

        mLayoutDecoder = view.findViewById(R.id.layout_decoder);
        mLayoutSkipTime = view.findViewById(R.id.layout_skip_time);
        mLayoutRotation = view.findViewById(R.id.layout_rotation);

        mTVDecoder = view.findViewById(R.id.tv_decoder_option);
        mTVSkipTime = view.findViewById(R.id.tv_skip_time);
        mTVRotation = view.findViewById(R.id.tv_rotation);
        mTVUsableSize = view.findViewById(R.id.tv_avail_size);

        mSWExtSD = view.findViewById(R.id.sw_use_extSD);

        // 디코더 설정
        mLayoutDecoder.setOnClickListener(this);
        /*mTVDecoder.setText(
                getResources().getTextArray(R.array.decoder_option)[ConfigurationManager.getDecoderMode(getContext())]);*/
        mTVDecoder.setText(
                IMGApplication.decoderModeName[ConfigurationManager.getDecoderMode(getContext())]);

        // 스킵 시간
        mLayoutSkipTime.setOnClickListener(this);
        mTVSkipTime.setText(ConfigurationManager.getFastSeekTime(getContext())
                + getResources().getString(R.string.seconds));

        // 화면 회전
        mLayoutRotation.setOnClickListener(this);
        mTVRotation.setText(
                getResources().getTextArray(R.array.rotation_option)[ConfigurationManager.getRotation(getContext())]);

        // 외장 SD 카드 설정
        mSWExtSD.setChecked(ConfigurationManager.getUseExtSDCard(getContext()));
        mSWExtSD.setOnCheckedChangeListener(this);

        setStorageSize();

        return view;
    }

    /**
     * 저장 공간의 전체 및 사용 가능 공간 TextView 설정
     */
    private void setStorageSize() {

        String strCard;
        if (ConfigurationManager.getUseExtSDCard(getContext())) {
            strCard = getString(R.string.settings_external_storage);
        } else {
            strCard = getString(R.string.settings_internal_storage);
        }

        if (mTVUsableSize != null) {

            mTVUsableSize.setText(String.format("%s %s GB "
                    + getString(R.string.settings_usable) + " / "
                    + getString(R.string.settings_total)
                    + " %s GB", strCard, Lib.formatFloat(
                    ContentFileManager.getInstance(
                            getActivity()).getCurrentUsableSpace() / (1024F * 1024F * 1024F)), Lib.formatFloat(
                    ContentFileManager.getInstance(
                            getActivity()).getCurrentTotalSpace() / (1024F * 1024F * 1024F)))
            );
        }
    }

    /**
     * 디코더 설정 표시
     */
    private void dialogDecoderSetting() {

        if (getActivity() == null) {
            return;
        }

        SingleChoiceDialog dialog = SingleChoiceDialog.getInstance(
                SingleChoiceDialog.DECODER_OPTION,
                ConfigurationManager.getDecoderMode(getContext()), this);
        dialog.show(getFragmentManager(), SingleChoiceDialog.DIALOG_TAG);
    }

    /**
     * 화면 회전 설정 표시
     */
    private void dialogRotationSetting() {

        if (getActivity() == null) {
            return;
        }

        SingleChoiceDialog dialog = SingleChoiceDialog.getInstance(
                SingleChoiceDialog.ROTATION_OPTION,
                ConfigurationManager.getRotation(getContext()), this);
        dialog.show(getFragmentManager(), SingleChoiceDialog.DIALOG_TAG);
    }

    /**
     * 스킵 설정 표시
     */
    private void dialogSkipSetting() {

        if (getActivity() == null) {
            return;
        }

        EditTextDialog dialog = EditTextDialog.getInstance(EditTextDialog.STATE_SEEK_TIME, this);
        dialog.show(getFragmentManager(), EditTextDialog.DIALOG_TAG);
    }

    /**
     * 다운로드 상태로 저장 공간 변경 불가 일림 표출
     */
    private void dialogDownloadStatus() {

        if (getActivity() == null) {
            return;
        }

        String msg = getString(R.string.unable_change_storage);

        DownloadDialog mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_STATUS, 0, msg, null);
        mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

}
