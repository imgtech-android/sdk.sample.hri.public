package kr.hri.sample.player.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import kr.hri.sample.player.R;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;

/**
 * DialogFragment 구현 Download 및 기본 화면 Dialog
 * @author kimsanghwan
 * @since 2014. 9. 13.
 */
public class DownloadDialog extends DialogFragment implements BaseInterface {

    public final static String DIALOG_TAG = "ZONEPLAYER_DIALOG";
    public final static int DIALOG_CHOICE_POSITIVE = 1;			// 확인 등 실행의 선택
    public final static int DIALOG_CHOICE_NEUTRAL = 0;			// 중립 선택
    public final static int DIALOG_CHOICE_NEGATIVE = -1;		// 아니오 등 거부의 선택
    public final static int DIALOG_CANCEL = 2;					// 버튼 선택없이 백버튼으로 취소

    private final static String KEY_STATE = "state";			// 다이얼로그 상태키
    private final static String KEY_CODE = "code";				// 표출 코드키
    private final static String KEY_MESSAGE = "message";		// 표출 메시지 키

    private int state;									// 다이얼로그 상태
    private int code;									// 전달된 코드
    private String message;								// 전달된 메시지
    private BaseDialogListener listener;				// 다이얼로그 선택을 받는 리스너

    /**
     * 다이얼로그 생성 및 반환
     * @param state	다이얼로그 상태
     * @return	DownloadDialog
     */
    public static DownloadDialog getInstance(int state) {
        DownloadDialog dialog = new DownloadDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * @param state			다이얼로그 상태
     * @param listener		BaseDialogListener
     * @return	DownloadDialog
     */
    public static DownloadDialog getInstance(int state, BaseDialogListener listener) {
        DownloadDialog dialog = new DownloadDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * 다이얼로그 화면에 특정 코드를 동시 표출
     * @param state			다이얼로그 상태
     * @param code			특정 코드
     * @param listener		BaseDialogListener
     * @return	DownloadDialog
     */
    public static DownloadDialog getInstance(int state, int code, BaseDialogListener listener) {
        DownloadDialog dialog = new DownloadDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * 다이얼로그 화면에 특정 코드 및 메시지 동시 표출
     * @param state		다이얼로그 상태
     * @param code		특정 코드
     * @param message	특정 메시지
     * @param listener	BaseDialogListener
     * @return	DownloadDialog
     */
    public static DownloadDialog getInstance(int state, int code, String message, BaseDialogListener listener) {
        DownloadDialog dialog = new DownloadDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);
        args.putString(KEY_MESSAGE, message);

        dialog.setArguments(args);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle arguments = getArguments();

        state = arguments.getInt(KEY_STATE);

        if (arguments.containsKey(KEY_CODE)) {	// 코드가 존재하면
            code = arguments.getInt(KEY_CODE);
        }

        if (arguments.containsKey(KEY_MESSAGE)) {	// 메시지가 존재하면
            message = arguments.getString(KEY_MESSAGE);
        }

        switch (state) {

            // 다운로드 전체 취소
            case DIALOG_DOWNLOAD_DELETE_ALL :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_cancel_all);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        //					listener.onDialog(state, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 다운로드 아이템 취소
            case DIALOG_DOWNLOAD_CANCEL_ITEM :
                builder.setTitle(R.string.dialog_title_finish_yes_no);
                builder.setMessage(getResources().getString(R.string.dialog_message_cancel_item, message));

                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 다운로드 콘텐츠 아이템 삭제
            case DIALOG_DOWNLOAD_DELETE_ITEM:

                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_delete_item);

                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 다운로드 완료
            case DIALOG_DOWNLOAD_COMPLETE:
                builder.setTitle(R.string.dialog_title_common);
                // 다운로드 컨텐츠 OO건 중 OO건 다운로드 실패하였습니다.
                // builder.setMessage(getResources().getString(R.string.dialog_message_download_all, message));
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 다운로드 취소 완료
            case DIALOG_DOWNLOAD_CANCEL:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_download_cancel);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 보관된 콘텐츠 없음
            case DIALOG_NO_DOWNLOAD_CONTENTS:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_no_content);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 네트워크 데이터 과금 알림
            case DIALOG_NETWORK_WARNING :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_network_warning);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 인증서 기간이 유효하지 않음
            case DIALOG_INVALID_CERT_TERM :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_invalid_cert_term);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 유효하지 않은 단말기기 Alert
            case DIALOG_DRM_ERROR :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, DIALOG_DRM_ERROR, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 업데이트 Alert
            case DIALOG_UPDATE_YES_NO :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_update_yes_no);
                builder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.finish, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 보관함 화면 이동 Alert
            case DIALOG_GO_FOLDER :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_go_folder);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 현재 진행 중 다운로드 없음
            case DIALOG_NO_DOWNLOAD :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_no_download);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 외장 SD 카드 없음
            case DIALOG_NO_EXT_SDCARD :
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_message_no_ext_sd);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 다운로드 삭제 확인
            case DIALOG_CONFIRM_DELETE_ACTIVE_DOWNLOAD:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_confirm_delete_download);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 활성 다운로드 포함한 전체 삭제 확인
            case DIALOG_CONFIRM_DELETE_ALL:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_confirm_delete_download);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 다운로드 삭제 확인
            case DIALOG_CONFIRM_DELETE_ITEM:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_confirm_delete);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 다운로드 개수 알림
            case DIALOG_DOWNLOAD_COUNT:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 유효 공간 부족 알림
            case DIALOG_UNUSABLE_SPACE:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_unusable_space);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 다른 활성 다운로드 있음
            case DIALOG_EXIST_ACTIVE_DOWNLOAD:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_exist_active_download);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 네트워크 오류 알림
            case DIALOG_NO_NETWORK:
                builder.setTitle("네트워크 오류");
                builder.setMessage("네트워크에 접속할 수 없습니다.\n네트워크 연결 상태를 확인해주세요.");
                builder.setPositiveButton("재시도", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 미디어 마운트 에러
            case DIALOG_MEDIA_MOUNT_ERROR:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_media_mount_error);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 저장소 위치 다름
            case DIALOG_NOT_MATCHING_LOCATION:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 다운로드 상태로 저장 상태 변경 불가
            case DIALOG_DOWNLOAD_STATUS:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 선택한 아이템 없음
            case DIALOG_NO_SELECT:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 오버레이 권한 알림
            case DIALOG_PERMISSION_OVERLAY_INFO:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage("다른 앱 위에 그리기를 허용하지 않으면 시청 중 일부 기능이 제한됩니다.\n다시 허용 화면으로 이동하시겠습니까?");
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

            // 권한 허용 필요 알림
            case DIALOG_PERMISSION_NEED:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_permission_need);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, code, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;

            // 기타 오류
            default:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(getResources().getString(R.string.dialog_message_error_finish));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;
        }

        return builder.create();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * 다이얼로그 표출시 백버튼 터치 이벤트
     * 백버튼 터치 시 이벤트 수신
     */
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        switch (state) {

            default:
                if (listener != null) {
                    listener.onDialog(state, 0, DIALOG_CANCEL);
                }
                break;
        }
    }

}
