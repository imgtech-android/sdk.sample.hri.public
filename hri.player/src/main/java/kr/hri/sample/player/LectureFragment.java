package kr.hri.sample.player;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Browser;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.andlib.helpers.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

import kr.co.imgtech.lib.zonedrm.CertificateInfo;
import kr.co.imgtech.lib.zonedrm.LibZoneDRM;
import kr.hri.sample.player.component.DividerItemDecoration;
import kr.hri.sample.player.component.ItemTouchHelperCallback;
import kr.hri.sample.player.component.LectureAdapter;
import kr.hri.sample.player.widget.DownloadDialog;
import kr.hri.sample.player.widget.ProgressingDialog;
import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.BookmarkData;
import kr.imgtech.lib.zoneplayer.data.IndexData;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.data.SubtitlesData;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.gui.video.ExoPlayer2Activity;
import kr.imgtech.lib.zoneplayer.gui.video.FFMPEGPlayerActivity;
import kr.imgtech.lib.zoneplayer.gui.video.MediaPlayer2Activity;
import kr.imgtech.lib.zoneplayer.gui.video.MediaPlayerActivity;
import kr.imgtech.lib.zoneplayer.gui.video.MediaTrackActivity;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.itemtouchhelper.ItemTouchHelperExtension;
import kr.imgtech.lib.zoneplayer.network.NetworkManager;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.Lib;
import kr.imgtech.lib.zoneplayer.util.StringUtil;
import kr.imgtech.lib.zoneplayer.widget.CircleImageView;

/**
 * 강의 Fragment
 * Created by kimsanghwan on 2017. 8. 25..
 */
public class LectureFragment extends Fragment implements IBackPressedListener,
        IntentDataDefine,
        BaseInterface,
        BaseDialogListener,
        LectureAdapter.LectureAdapterListener {

    @SuppressLint("StaticFieldLeak")
    private static LectureFragment instance;

    private ArrayList<ZoneDownloadData> mListLecture = new ArrayList<>();   // 강의 ArrayList
    private LectureAdapter mAdapter;                                        // 강의 Adapter
    private ItemTouchHelperExtension mItemTouchHelper;
    private ItemTouchHelperExtension.Callback mHelperCallback;

    private ContentsDatabase2 mDB;
    private ProgressingDialog mProgressingDialog;

    private Intent mIntent;
    private String mSiteID;
    private String mUserID;
    private String mCourseID;
    private String mRequestURL;
    private String mDeleteURL;

    private RecyclerView mLectureRecycledView;

    private int mScrolledY = 0;     // 이전 스크롤링 위치 저장

    /**
     * 생성자
     */
    public LectureFragment() {

        super();

        mIntent = null;
    }

    /**
     * instance 반환
     * @return  CourseFragment
     */
    public static LectureFragment getInstance() {

        if (instance == null) {
            instance = new LectureFragment();
        } else {
            instance.mIntent = null;
        }

        return instance;
    }

    /**
     * instance 반환
     * @param bundle    Bundle
     * @return  CourseFragment
     */
    public static LectureFragment getInstance(Bundle bundle) {

        instance = getInstance();

        if (bundle != null) {

            instance.mIntent = bundle.getParcelable(MainActivity.KEY_INTENT);
        }

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mDB = ContentsDatabase2.getInstance(getContext());

        return initView(inflater, container);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mAdapter != null) {

            if ((mIntent != null) && (mListLecture != null)) {

                Uri uri = mIntent.getData();
                if (uri != null) {

                    mSiteID = uri.getQueryParameter(SITE_ID);
                    mUserID = uri.getQueryParameter(USER_ID);
                    mCourseID = uri.getQueryParameter(COURSE_ID);
                    mRequestURL = uri.getQueryParameter(MainActivity.KEY_REQUEST_URL);
                    mDeleteURL = uri.getQueryParameter(MainActivity.KEY_DELETE_URL);

                    // 강의 목록 반환
                    ArrayList<ZoneDownloadData> lectureList = mDB.getAllFileInfoDetail(mSiteID, mCourseID);

                    mAdapter.updateList(lectureList);
                }
            }

            mLectureRecycledView.setScrollY(mScrolledY);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        new UpdateItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mAdapter.getCourseList());

        mScrolledY = mLectureRecycledView.getScrollY();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mListLecture != null) {
            mListLecture.clear();
        }

        if (mAdapter != null) {
            mAdapter = null;
        }
    }

    @Override
    public void onBackPressed() {

        ((MainActivity) getActivity()).switchBottomNavigationBar(MainActivity.ID_BOX_COURSE);

        Bundle bundle = new Bundle();
        bundle.putParcelable(MainActivity.KEY_INTENT, makeCourseIntent(mSiteID));
        ((MainActivity) getActivity()).switchFragment(MainActivity.ID_BOX_COURSE, bundle);
    }

    @Override
    public void onDialog(int state, int code, int result) {

        switch (state) {

            case DIALOG_DOWNLOAD_DELETE_ITEM:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {
                    //Lib.toaster(getContext(), "code: " + code);
                    new DeleteItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, code);
                }

                break;
        }
    }

    @Override
    public void onItemSelectedListener(int position) {

        ZoneDownloadData data = mAdapter.getItem(position);

        // 저장된 DB (이어보기 / 북마크 등 기타 정보)에서 실행 데이터 획득
        ZoneDownloadData cbData = ContentsDatabase2.getInstance(getActivity()).getFileInfoDetail(data.fileID);

        playContents(cbData);
    }

    @Override
    public void onDeleteSelectedListener(int position) {

        dialogDeleteItem(position);
    }

    private View initView(LayoutInflater in, ViewGroup viewGroup) {

        View view = in.inflate(R.layout.fragment_lecture, viewGroup, false);

        setCourseTitle(view);

        mLectureRecycledView = view.findViewById(R.id.rv_lecture);
        mLectureRecycledView.addItemDecoration(new DividerItemDecoration(getContext()));
        mLectureRecycledView.setHasFixedSize(true);

        mAdapter = new LectureAdapter(getContext());
        mAdapter.setOnSelectedListener(this);
        mLectureRecycledView.setAdapter(mAdapter);
        mLectureRecycledView.setLayoutManager(new LinearLayoutManager(getContext()));
        mLectureRecycledView.setItemAnimator(new DefaultItemAnimator());
        mHelperCallback = new ItemTouchHelperCallback();
        mItemTouchHelper = new ItemTouchHelperExtension(mHelperCallback);
        mItemTouchHelper.attachToRecyclerView(mLectureRecycledView);
        mAdapter.setItemTouchHelperExtension(mItemTouchHelper);
        mAdapter.setCourseList(mListLecture);

        return view;
    }

    /**
     * 강좌 타이틀 설정
     * @param parentView    Layout View
     */
    @SuppressLint("SetTextI18n")
    private void setCourseTitle(View parentView) {

        CircleImageView ivCourseThumb = parentView.findViewById(R.id.iv_course_thumb);
        TextView tvTeacher = parentView.findViewById(R.id.tv_teacher);
        TextView tvCourseName = parentView.findViewById(R.id.tv_course_name);
        TextView tvEndTime = parentView.findViewById(R.id.tv_end_time);

        String courseImage, teacher, courseName, isCert, endTime, remainTime;

        Uri uri = mIntent.getData();
        teacher = uri.getQueryParameter(TEACHER_NAME);
        courseName = uri.getQueryParameter(COURSE_NAME);
        isCert = uri.getQueryParameter(IS_CERT);
        endTime = uri.getQueryParameter(END_TIME);
        remainTime = uri.getQueryParameter("remain-time");
        courseImage = uri.getQueryParameter(COURSE_IMAGE);

        if (StringUtil.isNotBlank(courseImage)) {

            InputStream is;

            try {
                URL url = new URL("file://" + courseImage);
                is = url.openStream();
            } catch (IOException e) {
                e.printStackTrace();
                is = null;
            }

            if (is != null) {
                ivCourseThumb.setImageBitmap(BitmapFactory.decodeStream(is));
            }
        }

        tvTeacher.setText(teacher);
        tvCourseName.setText(courseName);

        if (StringUtil.equals(isCert, "1")) {

            if (StringUtil.isNotBlank(endTime) && (StringUtil.isNotBlank(remainTime))) {

                tvEndTime.setText(
                        getString(R.string.end_time) +  ": " + endTime
                                + " " + getString(R.string.remain_time) + ": " + remainTime + getString(R.string.day));
            } else {

                tvEndTime.setText("-");
            }

        } else {

            tvEndTime.setText(getString(R.string.no_expired));
        }

    }

    /**
     * Time 을 날짜 형식 문자로 반환
     * @param date	Time (초 단위)
     * @return 날짜 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDate(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return String.format("%04d.%02d.%02d", year, mon + 1, day);
    }

    /**
     * Today 0시 0분 0초 시간(TimeInMillis) 반환
     * @return	Today 0시 0분 0초 시간(TimeInMillis)
     */
    private long getTodayZeroHour() {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        cal.set(year, mon, day, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis() / 1000;
    }

    /**
     * Course Group Fragment Intent 생성
     * @param siteID	Site ID
     * @return	Course Group Fragment Intent
     */
    private Intent makeCourseIntent(String siteID) {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_hri))
                .authority("course-list")
                .appendQueryParameter(SITE_ID, siteID)
                .appendQueryParameter(COURSE_ID, "")
                ;

        Logger.d(uriBuilder.toString());

        Intent intent = null;

        // Intent 설정
        try {
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getContext().getPackageName());
            intent.setPackage(getContext().getPackageName());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return intent;
    }

    private void playContents(ZoneDownloadData data) {

        // 현재 콘텐츠 파일 path 가 내장 또는 외장인지 확인
        String path =
                (StringUtil.contains(data.filePath,
                        ContentFileManager.getInstance(getContext()).mExtSDCardPath)
                        ? ContentFileManager.getInstance(getContext()).mExtSDCardPath
                        : ContentFileManager.getInstance(getContext()).mIntSDCardPath);

        // 외장 유효 여부 확인 후 유효하지 않으면 오류 Alert 표출
        if (! ContentFileManager.getInstance(getContext()).isAvailSpace(path)) {

            dialogNoExtSDCard();
            return;
        }

        // 인증서 유효 기간 비교. 인증서 유효 기간에 포함되는지 확인
        // 인증서 필요 여부 비교. 인증서가 필요없는 파일인지 확인
        if (! isValidCert(data.filePath)
                && (data.isCert > 0)) {
            dialogInvalidCertTerm();
            return;
        }

        // 인증 파일이면 ZoneHttPD 를 설정하고 해당 URL 획득
        if (data.isCert > 0 ) {

            /*if (StringUtil.isNotBlank(data.drmURL) && StringUtil.isNotBlank(data.drmTime)) {

                if (ConfigurationManager.getAllowNonDRMAuth(getContext())) {

                    // 기기인증 없이 재생
                    new DRMAuthAsyncTask(false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

                } else {

                    // 기기인증 후 재생
                    new DRMAuthAsyncTask(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
                }

            } else {

                // 기기인증 없이 재생
                new DRMAuthAsyncTask(false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
            }*/

            String filepath = "http://localhost:" + IMGApplication.mZoneHttpDPort + data.filePath;

            // 다운로드 데이터를 재생 데이터 형식으로 변환
            startPlayIntent(data, filepath);

            // 미인증 파일이면 기본 URL 앞에 file scheme 을  추가
        } else {

            // 다운로드 데이터의 재생 경로 변환.
            String filepath = "file://" + data.filePath;

            // 다운로드 데이터를 재생 데이터 형식으로 변환
            startPlayIntent(data, filepath);
        }
    }

    /**
     * 인증서 유효시간 및 현재시간 비교
     * @param filePath	인증서를 확인할 콘텐츠 파일의 저장 경로
     * @return	true: 유효, false: 유효하지 않음
     */
    private boolean isValidCert(String filePath) {

        LibZoneDRM libZoneDRM = new LibZoneDRM(getContext());

        CertificateInfo ci = libZoneDRM.getLocalCertificate(filePath);

        // 인증서가 없으면 재생 취소
        if (ci == null) {
            return false;
        }

        long current = getTodayZeroHour();			// Today 0시 0분 0초 초단위 시간
//		long current = System.currentTimeMillis();	// 현재 1/1000초단위 시간
//		long current = getDateAddDay(2).getTime();	// 유효기간 Fake 테스트

        // 현재 시간이 유효 시작 시간보다 크고, 유효 종료 시간보다 작으면
        // true 반환. 아니면 false 반환
		/*(current > ci.startDate) && */
        return (current < ci.endDate);
    }

    /**
     * 다운로드 데이터를 재생 데이터 형식으로 변환하여 반환
     * @param data	ZoneDownloadData
     * @param playURL 일반 파일: file://로 시작하는 일반 파일 URL, DRM 파일: ZoneHttPD 서버가 인식하는 실제 재생 URL
     */
    private void startPlayIntent(ZoneDownloadData data, String playURL) {

		// Activity 에 Attach 되어 있지않으면
        // getResources()를 실행할 수 없으며, 플레이어 재생이 불가능하므로
        // 실행하지 않고 null 반환
        if (! isAdded()) {
            return;
        }

        // 플레이어 모드 확인
        int mode = ConfigurationManager.getDecoderMode(getContext());

        Intent i;
        switch (mode) {

            case 0:
            case 1:
            case 2:
                i = new Intent(getContext(), FFMPEGPlayerActivity.class);
                break;

            case 3:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    i = new Intent(getContext(), MediaTrackActivity.class);
                } else {
                    i = new Intent(getContext(), MediaPlayerActivity.class);
                }
                break;

            case 5:
                i = new Intent(getContext(), ExoPlayer2Activity.class);
                break;

            // Android Media Player mode
            default:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    i = new Intent(getContext(), MediaPlayer2Activity.class);
                } else {
                    i = new Intent(getContext(), MediaPlayerActivity.class);
                }
                break;
        }

        JSONObject jsonData = new JSONObject();
        String strQuery = null;
        try {
            jsonData.put(CODE, "1");
            jsonData.put(SITE_ID, data.siteID);
            jsonData.put(USER_ID, data.userID);
            jsonData.put(MRL, playURL);
            jsonData.put(PLAY_TITLE, data.lectureName);

            // 자막 설정
            if ((data.arrayListSubtitles != null) && (data.arrayListSubtitles.size() > 0)) {

                JSONArray jsonArraySubtitles = new JSONArray();
                for (SubtitlesData subtitles : data.arrayListSubtitles) {
                    JSONObject json = new JSONObject();
                    json.put(SUBTITLES_URL, subtitles.subtitlesURL);
                    json.put(SUBTITLES_PATH, subtitles.subtitlesPath);
                    json.put(SUBTITLES_CHARSET, subtitles.subtitlesCharSet);
                    jsonArraySubtitles.put(json);
                }
                jsonData.put(SUBTITLES_LIST, jsonArraySubtitles);
            }

            jsonData.put(LMS_URL, data.lmsURL);
            jsonData.put(LMS_TIME, data.lmsTime);
            jsonData.put(PLAY_CUR_TIME, data.playCurTime);
            jsonData.put(PROGRESS_TIME, data.progressTime);

            // 북마크 설정
            if ((data.arrayListBookmark != null) && (data.arrayListBookmark.size() > 0)) {

                JSONArray jsonArrayBookmark = new JSONArray();
                for (BookmarkData bookmark : data.arrayListBookmark) {
                    JSONObject json = new JSONObject();
                    json.put(TITLE, bookmark.title);
                    json.put(TIME, bookmark.startTime);
                    jsonArrayBookmark.put(json);
                }
                jsonData.put(BOOKMARK_LIST, jsonArrayBookmark);
            }

            // 인덱스 설정
            if ((data.arrayListIndex != null) && (data.arrayListIndex.size() > 0)) {

                JSONArray jsonArrayIndex = new JSONArray();
                for (IndexData bookmark : data.arrayListIndex) {
                    JSONObject json = new JSONObject();
                    json.put(TITLE, bookmark.title);
                    json.put(START_TIME, bookmark.startTime);
                    json.put(END_TIME, bookmark.endTime);
                    jsonArrayIndex.put(json);
                }
                jsonData.put(INDEX_LIST, jsonArrayIndex);
            }

            jsonData.put(EXT_INFO, data.extInfo);
            jsonData.put(TCD, data.tcd);
            jsonData.put(DRM_URL, data.drmURL);
            jsonData.put(DRM_TIME, data.drmTime);

            jsonData.put(FILE_ID, data.fileID);

            strQuery = URLEncoder.encode(jsonData.toString(), "UTF-8");

        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme(getString(R.string.scheme_hri))
                .authority(getString(R.string.host_download_play))
                .encodedQuery(strQuery);

        // 해당 Player 로 Intent 전달
        i.setAction(Intent.ACTION_VIEW);
        i.setData(uriBuilder.build());
        //i.setPackage(getPackageName());
        //i.setData(intent.getData());

        startActivity(i);
    }

    /**
     * 삭제 Alert 표출
     * @param position  삭제 아이템 position
     */
    private void dialogDeleteItem(int position) {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ITEM, position, this);
        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * 외장 SD 카드 없음 Alert 표출
     */
    private void dialogNoExtSDCard() {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_NO_EXT_SDCARD, this);
        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * 인증서 기간이 유효하지 않음 Alert 표출
     */
    private void dialogInvalidCertTerm() {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog = DownloadDialog.getInstance(DIALOG_INVALID_CERT_TERM, this);
        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * DRM Error 표출
     * @param message Alert 에 표출할 메시지
     */
    private void dialogDRMError(String message) {

        if (getActivity() == null) {
            return;
        }

        DownloadDialog dialog;
        if (StringUtil.isNotBlank(message)) {
            dialog = DownloadDialog.getInstance(DIALOG_DRM_ERROR, 0, message, this);
        } else {
            dialog = DownloadDialog.getInstance(DIALOG_DRM_ERROR, 0,
                    getActivity().getResources().getString(R.string.dialog_message_invalid_device), this);
        }

        dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * ProgressDialog 표출
     */
    private void dialogWaiting() {

        if (getActivity() == null) {
            return;
        }

        mProgressingDialog = ProgressingDialog.getInstance(0);
        mProgressingDialog.show(getActivity().getSupportFragmentManager(), ProgressingDialog.DIALOG_TAG);
    }

    /**
     * ProgressDialog 해제
     */
    private void dismissWaiting() {

        if (getActivity() == null) {
            return;
        }

        if (mProgressingDialog != null) {
            mProgressingDialog.dismiss();
            mProgressingDialog = null;
        }
    }

    /**
     * 보관함 강의 삭제 콜백
     * @param data      삭제한 강의 ZoneDownloadData
     */
    private void sendDeleteInfo(ZoneDownloadData data) {

        if (StringUtil.isBlank(mDeleteURL)) {
            return;
        }

        String encodedExtInfo = "";
        try {
            encodedExtInfo = URLEncoder.encode(data.extInfo, "EUC-KR");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringBuilder parameterString = new StringBuilder("?");
        parameterString.append(USER_ID + "=" + mUserID + "&");
        parameterString.append(EXT_INFO + "=" + encodedExtInfo);

        // URL + URL 인코딩 문자열 전송
        new NetworkManager(getContext()).httpGet(mDeleteURL + parameterString);
    }

    /**
     * 강좌 그룹 삭제 AsyncTask
     */
    private class DeleteItemAsyncTask extends AsyncTask<Integer, Void, ArrayList<ZoneDownloadData>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogWaiting();
        }

        @Override
        protected ArrayList<ZoneDownloadData> doInBackground(Integer... params) {

            ArrayList<ZoneDownloadData> listDelete = new ArrayList<>();

            ZoneDownloadData data = mAdapter.getItem(params[0]);

            if (data != null) {

                listDelete.add(data);
            }

            for (ZoneDownloadData deleteData : listDelete) {

                // 파일 삭제
                ContentFileManager.getInstance(getActivity()).deleteFile(deleteData.filePath);

                // DB 에서 강의 삭제
                ContentsDatabase2.getInstance(getActivity()).deleteFileInfoDetail(data);

                // 삭제 콜백
                sendDeleteInfo(data);

                // Item 별 처리 delay
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return listDelete;
        }

        @Override
        protected void onPostExecute(ArrayList<ZoneDownloadData> result) {
            super.onPostExecute(result);

            if (result != null) {

                // Adapter 삭제
                for (ZoneDownloadData data : result) {
                    mAdapter.removeItem(data);
                }
            }

            // 삭제 후 현재 강좌 그룹에서 남은 강의 있는지 확인
            if (mAdapter.getItemCount() == 0) {
                onBackPressed();
            }

            dismissWaiting();
        }
    }

    /**
     * 강좌 그룹 정보 업데이트 AsyncTask
     */
    private class UpdateItemAsyncTask extends AsyncTask<ArrayList<ZoneDownloadData>, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(ArrayList<ZoneDownloadData>... params) {

            if (params != null && params[0] != null) {

                int index = 0;
                ArrayList<ZoneDownloadData> it = (ArrayList<ZoneDownloadData>) params[0].clone();

                for (ZoneDownloadData data : it) {

                    data.lectureSeq = index++;

                    mDB.updateFileInfo(data);
                }
            }

            return null;
        }
    }

    /**
     * DRM 인증
     */
    private class DRMAuthAsyncTask extends AsyncTask<ZoneDownloadData, Void, JSONObject> {

        private boolean needDRMAuth;
        private ZoneDownloadData data;

        /**
         * 생성자
         *
         * @param needAuth DRM Auth 필요 여부
         */
        DRMAuthAsyncTask(boolean needAuth) {

            needDRMAuth = needAuth;
        }

        @Override
        protected JSONObject doInBackground(ZoneDownloadData... params) {

            if (! needDRMAuth) {

                data = params[0];

                return null;
            }

            if ((params != null) && (params[0] != null)) {

                data = params[0];

                NetworkManager netMgr = new NetworkManager(getContext());

                String url = makeDRMAuthURL(params[0]);

                if (url != null) {

                    try {
                        String res = netMgr.httpGet(url);

                        JSONObject retJson;
                        try {
                            retJson = new JSONObject(res);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return null;
                        }

                        return retJson;

                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            parseDRMAuthResponse(result);
        }

        /**
         * DRM 인증 URL 생성
         * @param data  ZoneDownloadData
         * @return  DRM 인증 URL
         */
        private String makeDRMAuthURL(ZoneDownloadData data) {

            String ret = null;
            String token = "?";

            if ((data != null)
                    && (StringUtil.isNotBlank(data.userID))
                    && (StringUtil.isNotBlank(data.drmURL))) {

                if (data.drmURL.contains(token)) {
                    token = "&";	// 기존 URL 에 연결된 데이터가 있으면 연결자 변경
                }

                String encodedExtInfo = "";
                // extInfo 에 한글이 있는 경우에 대한 처리.
                if (StringUtil.isNotBlank(data.extInfo)) {
                    try {
                        encodedExtInfo = URLEncoder.encode(data.extInfo, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        encodedExtInfo = data.extInfo;
                    }
                }

                // 서버에 재생 정보 요청 URL 생성
                ret = data.drmURL
                        + token + SITE_ID + "=" + data.siteID
                        + "&" + USER_ID + "=" + data.userID
                        + "&" + EXT_INFO + "=" + encodedExtInfo
                        + "&" + DEVICE_INFO + "=" + Lib.getDeviceUUID(getActivity())
                        + "&" + STATUS + "=0"	        // 재생전(0)
                        + "&" + MODE + "=" + MODE_DRM   // DRM 다운로드 파일
                        ;
            }

            return ret;
        }

        /**
         * DRM 인증 응답 파싱 및 실행
         * @param json  DRM 응답
         */
        private void parseDRMAuthResponse(JSONObject json) {

            if (needDRMAuth) {

                if (json != null) {

                    String code = null, message = null;

                    try {
                        if (json.has(CODE)) {
                            code = json.getString(CODE);
                        }
                        if (json.has(MESSAGE)) {
                            message = json.getString(MESSAGE);
                        }

                        if (StringUtil.equals(code, CODE_FALSE)) {
                            dialogDRMError(message);
                            return;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialogDRMError(getResources().getString(R.string.dialog_drm_auth_error) + e.getMessage());
                        return;
                    }
                } else {
                    dialogDRMError(getResources().getString(R.string.dialog_drm_auth_error));
                    return;
                }
            }

            String filepath = "http://localhost:" + IMGApplication.mZoneHttpDPort + data.filePath;

            // 다운로드 데이터를 재생 데이터 형식으로 변환
            startPlayIntent(data, filepath);
        }
    }

}
